<?php
/**
 *  Theme:
 *  File: header.php
 *  Author: the Guaranteed SEO team
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo("charset"); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="<?php bloginfo('url');?>/favicon.ico">
		<title><?php wp_title(''); ?></title>
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
	<div class="behind-nav-block topnavbar"></div>
	<nav role="navigation" class="navbar navbar-default">
		<div class="container hidden-sm hidden-xs">
			<div class="col-lg-10 col-md-12 col-centered">
				<?php if ( get_theme_mod( 'rockware_logo' ) ) : ?>
				    <div class='navbar-brand'>
				        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'rockware_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' id='logo'></a>
				    </div>
				<?php else : ?>
				    <hgroup>
				        <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
				        <h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
				    </hgroup>
				<?php endif; ?>
				<?php wp_nav_menu( array(
																	'container' => 'ul',
																	'theme_location' => 'main',
																	'menu_class'=>'list-inline nav navbar-nav navbar-right hidden-sm hidden-xs'
																) ); ?>
			</div>
		</div>

		<div class="fluid-container hidden-md hidden-lg">
			<div class="col-lg-10 col-md-12 col-centered">

						<button type="button" class="rw-navbar-toggle hidden-md hidden-lg">
								<span class="sr-only">Toggle navigation</span>
								<img id="burger" src="<?php echo get_template_directory_uri(); ?>/img/burger.svg" alt="Menu icon">
						</button>

						<?php if ( get_theme_mod( 'rockware_logo' ) ) : ?>
						    <div class='navbar-brand'>
						        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'rockware_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' id='logo'></a>
						    </div>
						<?php else : ?>
						    <hgroup>
						        <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
						        <h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
						    </hgroup>
						<?php endif; ?>

			</div>
		</div>
	</nav>
	<div id="wrapper" class="container-fluid no-gutter">



		<main role="main">
