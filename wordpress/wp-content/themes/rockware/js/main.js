//menu
jQuery(document).ready(function($) {
  var navToggle = $('.rw-navbar-toggle');
  var mobileMenuContainer = $('.mobile-menu-container');
  var mobileMenu = $('.mobile-menu');

  navToggle.on('click', function() {
    mobileMenuContainer.fadeIn(200);
    mobileMenu.addClass('show-menu');
  });

  mobileMenuContainer.on('click', function(e) {
    if (!$(e.target).hasClass('mobile-menu')) {
      mobileMenuContainer.delay(500).fadeOut(100);
      mobileMenu.removeClass('show-menu');
    }
  });
});
