<section id="about">
	<div class="container">
		<div class="col-lg-offset-1 col-md-6 col-sm-12">
			<h2><?php the_field('was_wir_leisten_title'); ?></h2>
			<p><?php the_field('was_wir_leisten_paragraph'); ?></p>
			<a href="#" class="button"><?php the_field('was_wir_leisten_button'); ?></a>
		</div>
		<div class="col-lg-5 col-md-6 col-sm-12">
			<img src="<?php the_field('was_wir_leisten_image'); ?>" alt="<?php the_field('was_wir_leisten_image_alt'); ?>" class="img-responsive">
		</div>
	</div>
</section>
