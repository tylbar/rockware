<section id="subfooter" class="hidden-xs hidden-sm">
	<div class="container no-gutter">
		<div class="col-lg-10 col-sm-12 col-centered">
					<div class="col-sm-6"><?php the_field('subf_first_paragraph'); ?></div>
					<div class="col-sm-6"><?php the_field('subf_second_paragraph'); ?></div>
		</div>
	</div>
</section>
