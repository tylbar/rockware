<?php if( have_rows('social_icons', 'option') ): ?>
  <ul class="social-icons hidden-xs">
    <?php while( have_rows('social_icons', 'option') ): the_row(); ?>
      <li><a href="http://<?php the_sub_field('social_link'); ?>" target="_blank"><img src="<?php the_sub_field('social_icon'); ?>" alt="<?php the_sub_field('social_alt'); ?>"></a></li>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>
