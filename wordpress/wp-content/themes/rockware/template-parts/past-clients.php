<div class="container">
	<div class="col-lg-10 col-md-12 col-centered">

		<section id="past-clients">
			<h3>HABEN WIR SCHON GEROCKT:</h3>
			<ul class="client-icons">
				<?php if( have_rows('past_clients', 'option')): ?>
					<?php while( have_rows('past_clients', 'option') ): the_row(); ?>
						<li class="client-icon"><img class="img-responsive" src="<?php the_sub_field('past_client_icons', 'option'); ?>" alt="<?php the_sub_field('past_client_icon_alt'); ?>"></li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
			<hr class='hidden-xs'>
		</section>
	</div>
</div>
