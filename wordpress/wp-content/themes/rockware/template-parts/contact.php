<section id="contact" style='background-image: url("<?php the_field('contact_background'); ?>")'>
	<div class="fluid-container">
		<div class="col-sm-8 col-md-7 col-centered">
			<h2><?php the_field('contact_title_first_line'); ?><br class="hidden-sm hidden-md hidden-lg"> <?php the_field('contact_title_second_line'); ?></h2>
			<p class="hidden-xs"><?php the_field('contact_paragraph'); ?></p>
			<form action="">
				<input type="text" placeholder="Ihr Name">
				<input type="text" placeholder="Ihre E-Mail-Adresse">
				<textarea placeholder="Was Können wir für Sie tun?" name="" id="" cols="30" rows="15"></textarea>
				<button class="button button-shadow"><?php the_field('contact_button_text'); ?></button>
			</form>
		</div>
	</div>
</section>
