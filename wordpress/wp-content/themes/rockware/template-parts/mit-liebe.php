<section id="mit-liebe" style='background-image: url("<?php the_field('mit_liebe_background'); ?>");'>
	<div class="container no-gutter">
		<div class="col-lg-10 col-md-12 col-centered no-gutter">
			<div class="col-md-7 col-sm-8">
				<h2><?php the_field('mit_liebe_title'); ?></h2>
				<p><?php the_field('mit_liebe_paragraph'); ?></p>
				<div class="row">
					<div class="col-sm-6">
						<a href="#" class="button button-shadow"><?php the_field('mit_liebe_button_text'); ?></a>
					</div>
					<div class="col-sm-6">
						<?php get_template_part('template-parts/social', 'icons'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
