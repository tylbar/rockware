<section id="work">
	<div class="container no-gutter">
		<div class="col-lg-10 col-md-12 col-centered">
			<h2><?php the_field('arbeiten_title'); ?></h2>
			<div class="row">
				<?php if(have_rows('arbeiten_items')):
					while (have_rows('arbeiten_items')): the_row(); ?>
						<div class="col-xs-6 col-sm-3">
							<a href="<?php the_sub_field('arbeiten_page_link'); ?>"><div class="work-item"><img src="<?php the_sub_field('arbeiten_page_thumbnail'); ?>" alt="<?php the_sub_field('arbeiten_page_thumbnail_alt'); ?>" class='img-responsive'></div></a>
						</div>
					<?php endwhile;
				endif; ?>
				<div class="col-xs-6 col-sm-3">
					<div class="work-item more">
						<h3>MEHR<br>REFERENZEN</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
