<section id="blog">
	<div class="container no-gutter">
		<div class="col-lg-10 col-md-12 col-centered">
			<h2>BLOG<span class="hidden-sm hidden-md hidden-lg"> ALLE BEITRÄGE</span><span class='hidden-xs'> ALLE BEITRÄGE ANSEHEN</span></h2>
			<div class="row">
				<div class="col-sm-10 col-md-4">
					<h4>Vestibulum Vulputate Ornare</h4>
					<p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus.</p>
					<h5>07.10.2015</h5>
				</div>
				<div class="col-sm-10 col-md-4">
					<h4>Lorem Sem Pharetra Quam</h4>
					<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut.</p>
					<h5>04.10.2015</h5>
				</div>
				<div class="col-sm-10 col-md-4">
					<h4>Mollis Magna Ultricies</h4>
					<p>Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras justo.</p>
					<h5>04.10.2015</h5>
				</div>
			</div>
		</div>
	</div>
</section>
