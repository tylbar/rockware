<div class="row">
  <div class="col-lg-10 col-md-12 col-centered">
    <ul class="past-clients-2">
      <?php if( have_rows('past_clients_2', 'option')): ?>
        <?php while( have_rows('past_clients_2', 'option') ): the_row(); ?>
          <li><img src="<?php the_sub_field('past_client_icons_2', 'option'); ?>" alt="<?php the_sub_field('past_client_icon_alt_2', 'option'); ?>"></li>
        <?php endwhile; ?>
      <?php endif; ?>
    </ul>
    <hr class='hidden-xs'>
  </div>
</div>
