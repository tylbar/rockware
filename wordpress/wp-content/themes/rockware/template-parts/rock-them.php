<div class="container">

		<section id="rock-them">
			<div class="row">
				<div class="col-lg-10 col-md-8 col-sm-10 col-centered">
					<h2><?php the_field('wir_title'); ?></h2>
					<p><?php the_field('wir_paragraph'); ?></p>
					<img id="people" src="<?php the_field('wir_image'); ?>" class="img-responsive" alt="<?php the_field('wir_image_alt'); ?>">
				</div>
			</div>
			<?php get_template_part('template-parts/past','clients2'); ?>
		</section>
</div>
