<?php if( have_rows('footer_social_icons', 'option') ): ?>
  <ul class="social-icons hidden-xs">
    <?php while( have_rows('footer_social_icons', 'option') ): the_row(); ?>
      <li><a href="http://<?php the_sub_field('social_link'); ?>" target='_blank'><img src="<?php the_sub_field('social_icon'); ?>" alt="<?php the_sub_field('social_alt'); ?>"></a></li>
    <?php endwhile; ?>
    <li><a href="javascript:window.print()"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/print.svg" alt="Print this page"></a></li>
  </ul>
<?php endif; ?>
