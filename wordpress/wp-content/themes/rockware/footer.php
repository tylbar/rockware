<?php
/**
 *  Theme:
 *  File: footer.php
 *  Author: the Guaranteed SEO team
 */
?>

	</main>

	<footer id="footer" class="hidden-xs hidden-sm">
		<div class="container">
			<div class="col-lg-10 no-gutter col-md-12 col-centered">
				<div class="col-sm-6 no-gutter">
					<p class="copyright">©2015 Rock<span>ware</span></p>
				</div>
				<div class="col-sm-6">
					<?php get_template_part('template-parts/footer', 'social-icons'); ?>
				</div>
			</div>
		</div>
	</footer>
</div> <!-- wrapper -->

<script>(function(){var method;var noop=function(){};var methods=['assert','clear','count','debug','dir','dirxml','error','exception','group','groupCollapsed','groupEnd','info','log','markTimeline','profile','profileEnd','table','time','timeEnd','timeStamp','trace','warn'];var length=methods.length;var console=(window.console=window.console||{});while(length--){method=methods[length];if(!console[method]){console[method]=noop;}}}());</script>
<?php wp_footer(); ?>

</body>
</html>
