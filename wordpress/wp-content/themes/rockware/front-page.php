<?php
/**
 *  Theme:
 *  File: front-page.php
 *  Author: Tyler Barnes
 */

get_header(); ?>

<section class='hero-unit' style='background: url("<?php the_field('header_image'); ?>"); background-position: center; background-size: cover;'>
	<div class="container">
		<div class="row">
			<div class='col-xs-6 col-sm-4 col-md-3 col-centered'>
				<h1><span class="header-large">HI</span><br><span class="header-medium">WIR SIND</span><br><span class="header-small highlight">ROCKWARE</span></h1>
			</div>
		</div>
		<div class="row hidden-xs">
			<div class="col-sm-4 col-md-3 col-centered">
				<hr>
				<h2><span class="header-large">UND WIR</span> <span class="header-small">ENTWICKELN</span> <span class="header-medium">SOFTWARE</span></h2>
			</div>
		</div>
	</div>
</section>
<div class="down-arrow">
	<a href="#past-clients"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/caret-down.svg" alt="Down indicator"></a>
</div>

<?php get_template_part('template-parts/past', 'clients'); ?>

<?php get_template_part('template-parts/about'); ?>

<?php get_template_part('template-parts/work'); ?>

<?php get_template_part('template-parts/mit', 'liebe'); ?>

<?php get_template_part('template-parts/rock', 'them'); ?>

<?php get_template_part('template-parts/small', 'blog'); ?>

<?php get_template_part('template-parts/contact'); ?>

<?php get_template_part('template-parts/subfooter'); ?>

<?php get_footer(); ?>
