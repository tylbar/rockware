<?php
/*
*Template Name: Blog
*/
get_header(); ?>

<section class='hero-unit' style='background: url("<?php the_field('header_image'); ?>"); background-position: center; background-size: cover;'>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1>BLOG</h1>
      </div>
    </div>
</section>
<div class='container'>
  <section class='col-xs-12 col-sm-12 col-lg-10 col-lg-offset-1 no-gutter'>
    <div class='row'>
      <section class="main col-xs-12 col-md-9">
        <section id="posts">
          <article class="post">
            <h2 class="post-title">LOREM IPSUM DOLOR SIT AMET, CONSETETUR SADIPSCING ELITR.</h2>
            <div class="featured-image"><img class='img-responsive' src="../img/blog-posts/desktop-flat.jpg" alt=""></div>
            <p class="excerpt">
              Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor
              ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.
              Cras mattis consectetur purus sit amet fermentum. Sed posuere consectetur est at lobortis.
              Donec sed odio dui. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec sed
              odio dui. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec ullamcorper
              nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla.
            </p>
            <a href="" class="button">WEITERLESEN</a>
            <hr>
          </article>

          <article class="post">
            <h2 class="post-title">Parturient Inceptos Risus</h2>
            <div class="featured-image"><img class='img-responsive' src="../img/blog-posts/laptop.jpg" alt=""></div>
            <p class="excerpt">
              Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.
            </p>
            <a href="" class="button">WEITERLESEN</a>
            <hr>
          </article>

          <article class="post">
            <h2 class="post-title">Ornare Dolor Pellentesque Aenean</h2>
            <div class="featured-image"><img class='img-responsive' src="../img/blog-posts/android.jpg" alt=""></div>
            <p class="excerpt">
                Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Nullam id dolor id nibh ultricies vehicula ut id elit. Nulla vitae elit libero, a pharetra augue.
            </p>
            <a href="" class="button">WEITERLESEN</a>
            <hr>
          </article>
        </section>

        <section class="pagi">
          <a href='#' class="prev">NEUERE BEITRÄGE</a>
          <a href='#' class="next available">ÄLTERE BEITRÄGE</a>
        </section>
      </section>

      <section class="blog-sidebar col-xs-12 col-md-3">
        <div class="widget-area">
          <div class="recent-posts widget">
            <h3>NEUESTE BEITRÄGE</h3>
            <div class='recent-post'>
              <h4 class='recent-post-title'>Cum sociis natoque penatibus et magnis.</h4>
              <h5 class="date">01.07.15</h5>
            </div>
            <div class='recent-post'>
              <h4 class='recent-post-title'>Cum sociis natoque penatibus et magnis.</h4>
              <h5 class="date">01.07.15</h5>
            </div>
            <div class='recent-post'>
              <h4 class='recent-post-title'>Cum sociis natoque penatibus et magnis.</h4>
              <h5 class="date">01.07.15</h5>
            </div>
          </div>
          <div class="archive widget">
            <h3>ARCHIV</h3>
            <ul>
              <li>Juli 2015</li>
              <li>Juni 2015</li>
              <li>Mai 2015</li>
            </ul>
          </div>
          <div class="tags widget">
            <h3>TAGS</h3>
            <ul>
              <li>hendrerit</li>
              <li>voldupscing</li>
              <li>Ipsum dolor</li>
              <li>Widiriamius</li>
              <li>Dolore et Labore</li>
            </ul>
          </div>
        </div>
      </section>
    </div>
  </section>
</div>

<?php
get_footer();
?>
