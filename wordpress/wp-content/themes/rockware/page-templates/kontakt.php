<?php
/*
*Template Name: Kontakt
*/
get_header(); ?>

<section class='hero-unit' style='background: url("<?php the_field('header_image'); ?>"); background-position: center; background-size: cover;'>
  <h4>ROCKWARE SITZT IN DER BALANSTRASSE 388, 81549 MÜNCHEN</h4>
</section>

<section id="text-thumb">
  <div class="container">
    <div class="col-lg-10 col-md-12 col-centered">
      <div class="row no-gutter">
        <div class="col-xs-12 col-sm-9 text">
          <h2>SAGEN SIE DOCH EINFACH MAL SERVUS.</h2>
          <p>Maecenas faucibus mollis interdum. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
  Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec sed odio dui. </p>
        </div>
        <div class="col-sm-2 col-xs-2 col-xs-offset-4 col-sm-offset-0 image">
          <div class="team-member">
            <div class="row">
              <img src="../img/team/team-member-1.jpg" alt="">
            </div>
            <div class="row">
              <h3>MARTIN EGDORF</h3>
              <h4>GECHÄFTSLEITER</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="contact">
  <div class="fluid-container">
    <div class="col-sm-8 col-md-7 col-centered">
      <h2>LUST AUF EINEN<br class="hidden-sm hidden-md hidden-lg"> PRIVATEN GIG?</h2>
      <p class="hidden-xs">Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet.</p>
      <form action="">
        <input type="text" placeholder="Ihr Name">
        <input type="text" placeholder="Ihre E-Mail-Adresse">
        <textarea placeholder="Was Können wir für Sie tun?" name="" id="" cols="30" rows="15"></textarea>
        <button class="button button-shadow">ROCK IT</button>
      </form>
    </div>
  </div>
</section>

<?php
get_footer();
?>
