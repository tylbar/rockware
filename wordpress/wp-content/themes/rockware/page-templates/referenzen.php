<?php
/*
*Template Name: Referenzen
*/
get_header(); ?>

<section class='hero-unit' style='background: url("<?php the_field('header_image'); ?>"); background-position: center; background-size: cover;'>
  <h1>UNSERE ARBEITEN</h1>
</section>

<div class="container">
    <div class="row">
      <div class="col-lg-10 col-md-12 col-centered">
        <ul class="past-clients-2">
          <li><img src="../icon/past-clients-2/bmw.png" alt=""></li>
          <li><img src="../icon/past-clients-2/siemens.png" alt=""></li>
          <li><img src="../icon/past-clients-2/ibm.png" alt=""></li>
          <li><img src="../icon/past-clients-2/playboy.png" alt=""></li>
          <li><img src="../icon/past-clients-2/zurich.png" alt=""></li>
          <li><img src="../icon/past-clients-2/google.png" alt=""></li>
        </ul>
        <hr class='hidden-xs'>
      </div>
    </div>
</div>

<section id="text-thumb">
  <div class="container">
    <div class="col-lg-10 col-md-12 col-centered">
      <div class="row no-gutter">
        <div class="col-xs-12 col-sm-9 text">
          <h2>SAGEN SIE DOCH EINFACH MAL SERVUS.</h2>
          <p>Maecenas faucibus mollis interdum. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
  Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec sed odio dui. </p>
        </div>
        <div class="col-sm-2 col-xs-2 col-xs-offset-4 col-sm-offset-0 image">
          <div class="team-member">
            <div class="row">
              <img src="../img/team/team-member-1.jpg" alt="">
            </div>
            <div class="row">
              <h3>MARTIN EGDORF</h3>
              <h4>GECHÄFTSLEITER</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section id="work">
  <div class="container no-gutter-sm">
    <div class="col-lg-10 col-md-12 col-centered">
      <h2>ARBEITEN</h2>
      <div class="row">
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="work-item"><img src="../img/arbeiten.jpg" alt=""></div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
?>
