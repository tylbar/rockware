<?php
/*
*Template Name: Über Uns
*/
get_header(); ?>

<section class='hero-unit' style='background: url("<?php the_field('header_image'); ?>"); background-position: center; background-size: cover;'>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1><?php the_field('header_title'); ?></h1>
      </div>
    </div>
</section>

<?php get_template_part('template-parts/past', 'clients'); ?>

<section id="mit-spass">
  <div class="container">
    <div class="col-lg-offset-1 col-lg-10 col-md-12 col-sm-12 col-centered">
      <h2>MIT SPASS &amp; ENERGIE BEI DER SACHE</h2>
      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.

Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. </p>
      <a href="#" class="button">SO HABEN WIR SCHON GEROCKT</a>
    </div>
  </div>
</section>

<section id="team">
  <div class="container">
    <div class="col-lg-offset-1 col-lg-10 col-sm-12 col-xs-11 col col-centered">
      <div class="row">
        <h2 class='hidden-xs'>HI. SO SEHEN WIR AUS.</h2>
        <h2 class='hidden-sm hidden-md hidden-lg'>SO SEHEN WIR AUS</h2>
      </div>
      <div class="row">
        <div class="col-xs-6 col-sm-3 team-member-container">
          <div class="team-member">
            <div class="row">
              <img src="../img/team/team-member-1.jpg" alt="">
            </div>
            <div class="row">
              <h3>MARTIN EGDORF</h3>
              <h4>GECHÄFTSLEITER</h4>
            </div>
          </div>
        </div>

        <div class="col-xs-6 col-sm-3 team-member-container">
          <div class="team-member">
            <div class="row">
              <img src="../img/team/team-member-1.jpg" alt="">
            </div>
            <div class="row">
              <h3>MARTIN EGDORF</h3>
              <h4>GECHÄFTSLEITER</h4>
            </div>
          </div>
        </div>

        <div class="col-xs-6 col-sm-3 team-member-container">
          <div class="team-member">
            <div class="row">
              <img src="../img/team/team-member-1.jpg" alt="">
            </div>
            <div class="row">
              <h3>MARTIN EGDORF</h3>
              <h4>GECHÄFTSLEITER</h4>
            </div>
          </div>
        </div>

        <div class="col-xs-6 col-sm-3 team-member-container">
          <div class="team-member">
            <div class="row">
              <img src="../img/team/team-member-1.jpg" alt="">
            </div>
            <div class="row">
              <h3>MARTIN EGDORF</h3>
              <h4>GECHÄFTSLEITER</h4>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<section id="und-hier">
  <h2 class='hidden-xs'>...UND HIER ARBEITEN WIR.</h2>
  <h2 class='hidden-sm hidden-md hidden-lg'>HIER ARBEITEN WIR</h2>
  <img src="../img/team/team-mobile.jpg" alt="" class='img-responsive hidden-sm hidden-md hidden-lg'>
  <img src="../img/team/team.jpg" alt="" class='img-responsive hidden-xs'>
</section>

<?php
get_footer();
?>
