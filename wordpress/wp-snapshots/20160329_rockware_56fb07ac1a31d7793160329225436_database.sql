/* DUPLICATOR MYSQL SCRIPT CREATED ON : 2016-03-29 22:54:43 */

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_duplicator_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `hash` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `owner` varchar(60) NOT NULL,
  `package` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=1212 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


/* INSERT TABLE DATA: wp_comments */
INSERT INTO `wp_comments` VALUES("1", "1", "Mr WordPress", "", "https://wordpress.org/", "", "2016-03-20 18:34:15", "2016-03-20 18:34:15", "Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.", "0", "1", "", "", "0", "0");

/* INSERT TABLE DATA: wp_duplicator_packages */
INSERT INTO `wp_duplicator_packages` VALUES("1", "20160329_rockware", "56fb07ac1a31d7793160329225436", "20", "2016-03-29 22:54:43", "tyler", "O:11:\"DUP_Package\":21:{s:2:\"ID\";i:1;s:4:\"Name\";s:17:\"20160329_rockware\";s:4:\"Hash\";s:29:\"56fb07ac1a31d7793160329225436\";s:8:\"NameHash\";s:47:\"20160329_rockware_56fb07ac1a31d7793160329225436\";s:7:\"Version\";s:5:\"1.1.6\";s:9:\"VersionWP\";s:5:\"4.4.2\";s:9:\"VersionDB\";s:6:\"5.5.42\";s:10:\"VersionPHP\";s:6:\"5.6.10\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:99:\"/Users/tylerbarnes/Google Drive/Web Design/Projects/Rockware/development/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://rockware:8888/wp-snapshots/\";s:8:\"ScanFile\";s:57:\"20160329_rockware_56fb07ac1a31d7793160329225436_scan.json\";s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";s:5:\"tyler\";s:7:\"Archive\";O:11:\"DUP_Archive\":13:{s:10:\"FilterDirs\";s:0:\"\";s:10:\"FilterExts\";s:0:\"\";s:13:\"FilterDirsAll\";a:0:{}s:13:\"FilterExtsAll\";a:0:{}s:8:\"FilterOn\";i:0;s:4:\"File\";s:59:\"20160329_rockware_56fb07ac1a31d7793160329225436_archive.zip\";s:6:\"Format\";s:3:\"ZIP\";s:7:\"PackDir\";s:82:\"/Users/tylerbarnes/Google Drive/Web Design/Projects/Rockware/development/wordpress\";s:4:\"Size\";i:0;s:4:\"Dirs\";a:0:{}s:5:\"Files\";a:0:{}s:10:\"FilterInfo\";O:23:\"DUP_Archive_Filter_Info\":6:{s:4:\"Dirs\";O:34:\"DUP_Archive_Filter_Scope_Directory\":4:{s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:5:\"Files\";O:29:\"DUP_Archive_Filter_Scope_File\":5:{s:4:\"Size\";a:0:{}s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:4:\"Exts\";O:29:\"DUP_Archive_Filter_Scope_Base\":2:{s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:9:\"UDirCount\";i:0;s:10:\"UFileCount\";i:0;s:9:\"UExtCount\";i:0;}s:10:\"\0*\0Package\";O:11:\"DUP_Package\":21:{s:2:\"ID\";N;s:4:\"Name\";s:17:\"20160329_rockware\";s:4:\"Hash\";s:29:\"56fb07ac1a31d7793160329225436\";s:8:\"NameHash\";s:47:\"20160329_rockware_56fb07ac1a31d7793160329225436\";s:7:\"Version\";s:5:\"1.1.6\";s:9:\"VersionWP\";s:5:\"4.4.2\";s:9:\"VersionDB\";s:6:\"5.5.42\";s:10:\"VersionPHP\";s:6:\"5.6.10\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:99:\"/Users/tylerbarnes/Google Drive/Web Design/Projects/Rockware/development/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://rockware:8888/wp-snapshots/\";s:8:\"ScanFile\";N;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";N;s:7:\"Archive\";r:20;s:9:\"Installer\";O:13:\"DUP_Installer\":12:{s:4:\"File\";s:61:\"20160329_rockware_56fb07ac1a31d7793160329225436_installer.php\";s:4:\"Size\";i:0;s:10:\"OptsDBHost\";s:0:\"\";s:10:\"OptsDBPort\";s:0:\"\";s:10:\"OptsDBName\";s:0:\"\";s:10:\"OptsDBUser\";s:0:\"\";s:12:\"OptsSSLAdmin\";i:0;s:12:\"OptsSSLLogin\";i:0;s:11:\"OptsCacheWP\";i:0;s:13:\"OptsCachePath\";i:0;s:10:\"OptsURLNew\";s:0:\"\";s:10:\"\0*\0Package\";r:50;}s:8:\"Database\";O:12:\"DUP_Database\":12:{s:4:\"Type\";s:5:\"MySQL\";s:4:\"Size\";N;s:4:\"File\";s:60:\"20160329_rockware_56fb07ac1a31d7793160329225436_database.sql\";s:4:\"Path\";N;s:12:\"FilterTables\";s:0:\"\";s:8:\"FilterOn\";i:0;s:4:\"Name\";N;s:10:\"Compatible\";s:0:\"\";s:10:\"\0*\0Package\";r:1;s:25:\"\0DUP_Database\0dbStorePath\";N;s:23:\"\0DUP_Database\0EOFMarker\";s:0:\"\";s:26:\"\0DUP_Database\0networkFlush\";b:0;}}}s:9:\"Installer\";r:70;s:8:\"Database\";r:83;}");

/* INSERT TABLE DATA: wp_options */
INSERT INTO `wp_options` VALUES("1", "siteurl", "http://rockware:8888", "yes");
INSERT INTO `wp_options` VALUES("2", "home", "http://rockware:8888", "yes");
INSERT INTO `wp_options` VALUES("3", "blogname", "rockware", "yes");
INSERT INTO `wp_options` VALUES("4", "blogdescription", "", "yes");
INSERT INTO `wp_options` VALUES("5", "users_can_register", "0", "yes");
INSERT INTO `wp_options` VALUES("6", "admin_email", "tyler@known.design", "yes");
INSERT INTO `wp_options` VALUES("7", "start_of_week", "1", "yes");
INSERT INTO `wp_options` VALUES("8", "use_balanceTags", "0", "yes");
INSERT INTO `wp_options` VALUES("9", "use_smilies", "1", "yes");
INSERT INTO `wp_options` VALUES("10", "require_name_email", "1", "yes");
INSERT INTO `wp_options` VALUES("11", "comments_notify", "1", "yes");
INSERT INTO `wp_options` VALUES("12", "posts_per_rss", "10", "yes");
INSERT INTO `wp_options` VALUES("13", "rss_use_excerpt", "0", "yes");
INSERT INTO `wp_options` VALUES("14", "mailserver_url", "mail.example.com", "yes");
INSERT INTO `wp_options` VALUES("15", "mailserver_login", "login@example.com", "yes");
INSERT INTO `wp_options` VALUES("16", "mailserver_pass", "password", "yes");
INSERT INTO `wp_options` VALUES("17", "mailserver_port", "110", "yes");
INSERT INTO `wp_options` VALUES("18", "default_category", "1", "yes");
INSERT INTO `wp_options` VALUES("19", "default_comment_status", "open", "yes");
INSERT INTO `wp_options` VALUES("20", "default_ping_status", "open", "yes");
INSERT INTO `wp_options` VALUES("21", "default_pingback_flag", "1", "yes");
INSERT INTO `wp_options` VALUES("22", "posts_per_page", "10", "yes");
INSERT INTO `wp_options` VALUES("23", "date_format", "F j, Y", "yes");
INSERT INTO `wp_options` VALUES("24", "time_format", "g:i a", "yes");
INSERT INTO `wp_options` VALUES("25", "links_updated_date_format", "F j, Y g:i a", "yes");
INSERT INTO `wp_options` VALUES("26", "comment_moderation", "0", "yes");
INSERT INTO `wp_options` VALUES("27", "moderation_notify", "1", "yes");
INSERT INTO `wp_options` VALUES("28", "permalink_structure", "/%year%/%monthnum%/%day%/%postname%/", "yes");
INSERT INTO `wp_options` VALUES("29", "hack_file", "0", "yes");
INSERT INTO `wp_options` VALUES("30", "blog_charset", "UTF-8", "yes");
INSERT INTO `wp_options` VALUES("31", "moderation_keys", "", "no");
INSERT INTO `wp_options` VALUES("32", "active_plugins", "a:2:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:25:\"duplicator/duplicator.php\";}", "yes");
INSERT INTO `wp_options` VALUES("33", "category_base", "", "yes");
INSERT INTO `wp_options` VALUES("34", "ping_sites", "http://rpc.pingomatic.com/", "yes");
INSERT INTO `wp_options` VALUES("35", "comment_max_links", "2", "yes");
INSERT INTO `wp_options` VALUES("36", "gmt_offset", "0", "yes");
INSERT INTO `wp_options` VALUES("37", "default_email_category", "1", "yes");
INSERT INTO `wp_options` VALUES("38", "recently_edited", "", "no");
INSERT INTO `wp_options` VALUES("39", "template", "rockware", "yes");
INSERT INTO `wp_options` VALUES("40", "stylesheet", "rockware", "yes");
INSERT INTO `wp_options` VALUES("41", "comment_whitelist", "1", "yes");
INSERT INTO `wp_options` VALUES("42", "blacklist_keys", "", "no");
INSERT INTO `wp_options` VALUES("43", "comment_registration", "0", "yes");
INSERT INTO `wp_options` VALUES("44", "html_type", "text/html", "yes");
INSERT INTO `wp_options` VALUES("45", "use_trackback", "0", "yes");
INSERT INTO `wp_options` VALUES("46", "default_role", "subscriber", "yes");
INSERT INTO `wp_options` VALUES("47", "db_version", "35700", "yes");
INSERT INTO `wp_options` VALUES("48", "uploads_use_yearmonth_folders", "1", "yes");
INSERT INTO `wp_options` VALUES("49", "upload_path", "", "yes");
INSERT INTO `wp_options` VALUES("50", "blog_public", "1", "yes");
INSERT INTO `wp_options` VALUES("51", "default_link_category", "2", "yes");
INSERT INTO `wp_options` VALUES("52", "show_on_front", "page", "yes");
INSERT INTO `wp_options` VALUES("53", "tag_base", "", "yes");
INSERT INTO `wp_options` VALUES("54", "show_avatars", "1", "yes");
INSERT INTO `wp_options` VALUES("55", "avatar_rating", "G", "yes");
INSERT INTO `wp_options` VALUES("56", "upload_url_path", "", "yes");
INSERT INTO `wp_options` VALUES("57", "thumbnail_size_w", "150", "yes");
INSERT INTO `wp_options` VALUES("58", "thumbnail_size_h", "150", "yes");
INSERT INTO `wp_options` VALUES("59", "thumbnail_crop", "1", "yes");
INSERT INTO `wp_options` VALUES("60", "medium_size_w", "300", "yes");
INSERT INTO `wp_options` VALUES("61", "medium_size_h", "300", "yes");
INSERT INTO `wp_options` VALUES("62", "avatar_default", "mystery", "yes");
INSERT INTO `wp_options` VALUES("63", "large_size_w", "1024", "yes");
INSERT INTO `wp_options` VALUES("64", "large_size_h", "1024", "yes");
INSERT INTO `wp_options` VALUES("65", "image_default_link_type", "none", "yes");
INSERT INTO `wp_options` VALUES("66", "image_default_size", "", "yes");
INSERT INTO `wp_options` VALUES("67", "image_default_align", "", "yes");
INSERT INTO `wp_options` VALUES("68", "close_comments_for_old_posts", "0", "yes");
INSERT INTO `wp_options` VALUES("69", "close_comments_days_old", "14", "yes");
INSERT INTO `wp_options` VALUES("70", "thread_comments", "1", "yes");
INSERT INTO `wp_options` VALUES("71", "thread_comments_depth", "5", "yes");
INSERT INTO `wp_options` VALUES("72", "page_comments", "0", "yes");
INSERT INTO `wp_options` VALUES("73", "comments_per_page", "50", "yes");
INSERT INTO `wp_options` VALUES("74", "default_comments_page", "newest", "yes");
INSERT INTO `wp_options` VALUES("75", "comment_order", "asc", "yes");
INSERT INTO `wp_options` VALUES("76", "sticky_posts", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("77", "widget_categories", "a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("78", "widget_text", "a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("79", "widget_rss", "a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("80", "uninstall_plugins", "a:0:{}", "no");
INSERT INTO `wp_options` VALUES("81", "timezone_string", "", "yes");
INSERT INTO `wp_options` VALUES("82", "page_for_posts", "0", "yes");
INSERT INTO `wp_options` VALUES("83", "page_on_front", "5", "yes");
INSERT INTO `wp_options` VALUES("84", "default_post_format", "0", "yes");
INSERT INTO `wp_options` VALUES("85", "link_manager_enabled", "0", "yes");
INSERT INTO `wp_options` VALUES("86", "finished_splitting_shared_terms", "1", "yes");
INSERT INTO `wp_options` VALUES("87", "site_icon", "0", "yes");
INSERT INTO `wp_options` VALUES("88", "medium_large_size_w", "768", "yes");
INSERT INTO `wp_options` VALUES("89", "medium_large_size_h", "0", "yes");
INSERT INTO `wp_options` VALUES("90", "initial_db_version", "35700", "yes");
INSERT INTO `wp_options` VALUES("91", "wp_user_roles", "a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}", "yes");
INSERT INTO `wp_options` VALUES("92", "widget_search", "a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("93", "widget_recent-posts", "a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("94", "widget_recent-comments", "a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("95", "widget_archives", "a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("96", "widget_meta", "a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("97", "sidebars_widgets", "a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:18:\"orphaned_widgets_1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}", "yes");
INSERT INTO `wp_options` VALUES("99", "widget_pages", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("100", "widget_calendar", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("101", "widget_tag_cloud", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("102", "widget_nav_menu", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("103", "cron", "a:4:{i:1459293773;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1459319655;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1459362866;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}", "yes");
INSERT INTO `wp_options` VALUES("108", "_site_transient_update_core", "O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.4.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.4.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.4.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.4.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.4.2\";s:7:\"version\";s:5:\"4.4.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.4\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1459292054;s:15:\"version_checked\";s:5:\"4.4.2\";s:12:\"translations\";a:0:{}}", "yes");
INSERT INTO `wp_options` VALUES("113", "_site_transient_update_themes", "O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1459292051;s:7:\"checked\";a:1:{s:8:\"rockware\";s:0:\"\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}", "yes");
INSERT INTO `wp_options` VALUES("118", "can_compress_scripts", "1", "yes");
INSERT INTO `wp_options` VALUES("135", "theme_mods_twentysixteen", "a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1458498948;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}", "yes");
INSERT INTO `wp_options` VALUES("136", "current_theme", "", "yes");
INSERT INTO `wp_options` VALUES("137", "theme_mods_rockware", "a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:4:\"main\";i:2;}s:13:\"rockware_logo\";s:56:\"http://rockware:8888/wp-content/uploads/2016/03/logo.png\";}", "yes");
INSERT INTO `wp_options` VALUES("138", "theme_switched", "", "yes");
INSERT INTO `wp_options` VALUES("161", "_site_transient_timeout_available_translations", "1458613532", "yes");
INSERT INTO `wp_options` VALUES("162", "_site_transient_available_translations", "a:77:{s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-07 13:09:53\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-12 10:15:45\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-07 20:53:51\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-11 22:42:10\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 08:50:29\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:22:\"Продължение\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-08 13:17:04\";s:12:\"english_name\";s:7:\"Bengali\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-04 09:40:25\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-16 13:48:03\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-16 15:34:57\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-26 16:01:40\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 22:48:20\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Forts&#230;t\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 14:19:21\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-26 16:11:56\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.4.2/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-29 10:47:54\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-01 18:27:32\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-07 04:39:48\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-15 11:52:35\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 13:34:17\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-06 23:10:59\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-14 21:14:29\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-25 13:07:29\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-24 15:17:36\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/es_PE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-13 06:14:13\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/es_VE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-09 18:08:52\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/es_GT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-13 12:28:49\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/es_ES.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"es\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-07 17:35:10\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/es_MX.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:6:\"4.3-RC\";s:7:\"updated\";s:19:\"2015-08-04 06:10:33\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.3-RC/es_CO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-23 00:46:01\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/es_AR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-02 20:27:44\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/es_CL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"es\";i:2;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-18 06:44:22\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-03 10:31:09\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-31 19:24:20\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-23 06:49:15\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 13:47:35\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-08 17:19:17\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-09 02:16:19\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.3.3\";s:7:\"updated\";s:19:\"2015-09-24 15:25:30\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.3.3/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-13 16:48:03\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-16 18:59:27\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"להמשיך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-14 20:26:52\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-04 08:39:26\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-03 14:37:42\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Tovább\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-04 07:13:54\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-21 16:17:50\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 00:20:24\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-13 13:43:58\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-25 13:08:14\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-09 08:53:31\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-24 00:12:01\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 20:07:24\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-28 05:41:39\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.10\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.10/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ေဆာင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-07 10:01:09\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-20 13:35:50\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.4.2/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-23 18:59:13\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-14 12:19:44\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-08 16:21:37\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-26 19:07:19\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.10\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.10/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"دوام\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-18 15:58:21\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-03-03 17:32:29\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-24 11:09:36\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-02 00:04:31\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-26 11:29:13\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-11-26 00:00:18\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Nadaljujte\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-02-23 10:30:30\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-09 09:09:51\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 23:28:56\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-08 03:22:55\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-11-27 15:51:36\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-17 23:12:27\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:6:\"4.1.10\";s:7:\"updated\";s:19:\"2015-03-26 16:45:38\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:9:\"Uyƣurqə\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.10/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2016-01-03 22:04:41\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-09 01:01:25\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.4.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-11 18:51:41\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-12 22:55:08\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.4.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}", "yes");
INSERT INTO `wp_options` VALUES("163", "nav_menu_options", "a:2:{i:0;b:0;s:8:\"auto_add\";a:1:{i:0;i:2;}}", "yes");
INSERT INTO `wp_options` VALUES("164", "rewrite_rules", "a:79:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=5&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}", "yes");
INSERT INTO `wp_options` VALUES("191", "recently_activated", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("192", "acf_version", "5.2.9", "yes");
INSERT INTO `wp_options` VALUES("227", "_site_transient_timeout_browser_b516786e573a426eb842ec2132ed35e2", "1459711876", "yes");
INSERT INTO `wp_options` VALUES("228", "_site_transient_browser_b516786e573a426eb842ec2132ed35e2", "a:9:{s:8:\"platform\";s:9:\"Macintosh\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"49.0.2623.87\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}", "yes");
INSERT INTO `wp_options` VALUES("248", "_transient_timeout_plugin_slugs", "1459378453", "no");
INSERT INTO `wp_options` VALUES("249", "_transient_plugin_slugs", "a:2:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:25:\"duplicator/duplicator.php\";}", "no");
INSERT INTO `wp_options` VALUES("253", "_transient_timeout_acf_pro_get_remote_info", "1459299183", "no");
INSERT INTO `wp_options` VALUES("254", "_transient_acf_pro_get_remote_info", "0", "no");
INSERT INTO `wp_options` VALUES("257", "_site_transient_timeout_theme_roots", "1459293783", "yes");
INSERT INTO `wp_options` VALUES("258", "_site_transient_theme_roots", "a:1:{s:8:\"rockware\";s:7:\"/themes\";}", "yes");
INSERT INTO `wp_options` VALUES("259", "_transient_timeout_dash_88ae138922fe95674369b1cb3d215a2b", "1459335186", "no");
INSERT INTO `wp_options` VALUES("260", "_transient_dash_88ae138922fe95674369b1cb3d215a2b", "<div class=\"rss-widget\"><p><strong>RSS Error</strong>: WP HTTP Error: Couldn\'t resolve host \'wordpress.org\'</p></div><div class=\"rss-widget\"><p><strong>RSS Error</strong>: WP HTTP Error: Couldn\'t resolve host \'planet.wordpress.org\'</p></div><div class=\"rss-widget\"><ul></ul></div>", "no");
INSERT INTO `wp_options` VALUES("261", "_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a", "1459302841", "yes");
INSERT INTO `wp_options` VALUES("262", "_site_transient_poptags_40cd750bba9870f18aada2478b24840a", "a:100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";s:4:\"5798\";}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"Post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";s:4:\"3602\";}s:6:\"plugin\";a:3:{s:4:\"name\";s:6:\"plugin\";s:4:\"slug\";s:6:\"plugin\";s:5:\"count\";s:4:\"3565\";}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";s:4:\"3069\";}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";s:4:\"2761\";}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";s:4:\"2286\";}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";s:4:\"2191\";}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";s:4:\"2062\";}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";s:4:\"2006\";}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";s:4:\"1984\";}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";s:4:\"1967\";}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";s:4:\"1919\";}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";s:4:\"1842\";}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"Facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";s:4:\"1651\";}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";s:4:\"1568\";}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";s:4:\"1550\";}s:9:\"wordpress\";a:3:{s:4:\"name\";s:9:\"wordpress\";s:4:\"slug\";s:9:\"wordpress\";s:5:\"count\";s:4:\"1521\";}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";s:4:\"1349\";}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";s:4:\"1290\";}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";s:4:\"1281\";}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";s:4:\"1193\";}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";s:4:\"1089\";}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";s:4:\"1057\";}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";s:4:\"1002\";}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";s:3:\"963\";}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";s:3:\"947\";}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";s:3:\"912\";}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";s:3:\"901\";}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"AJAX\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";s:3:\"899\";}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";s:3:\"885\";}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";s:3:\"882\";}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";s:3:\"828\";}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";s:3:\"804\";}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";s:3:\"785\";}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";s:3:\"759\";}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";s:3:\"753\";}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";s:3:\"744\";}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";s:3:\"741\";}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";s:3:\"741\";}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";s:3:\"739\";}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"Share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";s:3:\"732\";}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";s:3:\"730\";}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";s:3:\"693\";}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";s:3:\"687\";}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";s:3:\"679\";}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";s:3:\"675\";}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"CSS\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";s:3:\"670\";}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";s:3:\"667\";}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";s:3:\"648\";}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";s:3:\"638\";}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";s:3:\"637\";}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";s:3:\"631\";}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";s:3:\"610\";}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";s:3:\"603\";}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";s:3:\"594\";}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";s:3:\"588\";}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";s:3:\"587\";}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";s:3:\"586\";}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";s:3:\"584\";}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";s:3:\"575\";}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";s:3:\"565\";}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";s:3:\"558\";}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";s:3:\"553\";}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";s:3:\"544\";}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";s:3:\"539\";}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";s:3:\"527\";}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";s:3:\"523\";}s:7:\"picture\";a:3:{s:4:\"name\";s:7:\"picture\";s:4:\"slug\";s:7:\"picture\";s:5:\"count\";s:3:\"513\";}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";s:3:\"509\";}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";s:3:\"508\";}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";s:3:\"496\";}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";s:3:\"496\";}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";s:3:\"493\";}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";s:3:\"486\";}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";s:3:\"468\";}s:8:\"pictures\";a:3:{s:4:\"name\";s:8:\"pictures\";s:4:\"slug\";s:8:\"pictures\";s:5:\"count\";s:3:\"457\";}s:4:\"shop\";a:3:{s:4:\"name\";s:4:\"shop\";s:4:\"slug\";s:4:\"shop\";s:5:\"count\";s:3:\"450\";}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";s:3:\"440\";}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";s:3:\"439\";}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";s:3:\"437\";}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";s:3:\"436\";}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";s:3:\"435\";}s:5:\"flash\";a:3:{s:4:\"name\";s:5:\"flash\";s:4:\"slug\";s:5:\"flash\";s:5:\"count\";s:3:\"423\";}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";s:3:\"418\";}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";s:3:\"416\";}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";s:3:\"413\";}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";s:3:\"409\";}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";s:3:\"407\";}s:3:\"tag\";a:3:{s:4:\"name\";s:3:\"tag\";s:4:\"slug\";s:3:\"tag\";s:5:\"count\";s:3:\"407\";}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"News\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";s:3:\"404\";}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";s:3:\"399\";}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";s:3:\"396\";}s:9:\"thumbnail\";a:3:{s:4:\"name\";s:9:\"thumbnail\";s:4:\"slug\";s:9:\"thumbnail\";s:5:\"count\";s:3:\"393\";}s:6:\"upload\";a:3:{s:4:\"name\";s:6:\"upload\";s:4:\"slug\";s:6:\"upload\";s:5:\"count\";s:3:\"388\";}s:7:\"sharing\";a:3:{s:4:\"name\";s:7:\"sharing\";s:4:\"slug\";s:7:\"sharing\";s:5:\"count\";s:3:\"387\";}s:6:\"paypal\";a:3:{s:4:\"name\";s:6:\"paypal\";s:4:\"slug\";s:6:\"paypal\";s:5:\"count\";s:3:\"387\";}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";s:3:\"387\";}s:4:\"text\";a:3:{s:4:\"name\";s:4:\"text\";s:4:\"slug\";s:4:\"text\";s:5:\"count\";s:3:\"387\";}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";s:3:\"386\";}s:8:\"lightbox\";a:3:{s:4:\"name\";s:8:\"lightbox\";s:4:\"slug\";s:8:\"lightbox\";s:5:\"count\";s:3:\"385\";}}", "yes");
INSERT INTO `wp_options` VALUES("264", "_site_transient_update_plugins", "O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1459292052;s:7:\"checked\";a:2:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.2.9\";s:25:\"duplicator/duplicator.php\";s:5:\"1.1.6\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:1:{s:25:\"duplicator/duplicator.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"22600\";s:4:\"slug\";s:10:\"duplicator\";s:6:\"plugin\";s:25:\"duplicator/duplicator.php\";s:11:\"new_version\";s:5:\"1.1.6\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/duplicator/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/duplicator.1.1.6.zip\";}}}", "yes");
INSERT INTO `wp_options` VALUES("265", "duplicator_settings", "a:10:{s:7:\"version\";s:5:\"1.1.6\";s:18:\"uninstall_settings\";b:1;s:15:\"uninstall_files\";b:1;s:16:\"uninstall_tables\";b:1;s:13:\"package_debug\";b:0;s:17:\"package_mysqldump\";b:0;s:22:\"package_mysqldump_path\";s:0:\"\";s:24:\"package_phpdump_qrylimit\";s:3:\"100\";s:17:\"package_zip_flush\";b:0;s:20:\"storage_htaccess_off\";b:0;}", "yes");
INSERT INTO `wp_options` VALUES("266", "duplicator_version_plugin", "1.1.6", "yes");
INSERT INTO `wp_options` VALUES("267", "duplicator_package_active", "O:11:\"DUP_Package\":21:{s:2:\"ID\";N;s:4:\"Name\";s:17:\"20160329_rockware\";s:4:\"Hash\";s:29:\"56fb07ac1a31d7793160329225436\";s:8:\"NameHash\";s:47:\"20160329_rockware_56fb07ac1a31d7793160329225436\";s:7:\"Version\";s:5:\"1.1.6\";s:9:\"VersionWP\";s:5:\"4.4.2\";s:9:\"VersionDB\";s:6:\"5.5.42\";s:10:\"VersionPHP\";s:6:\"5.6.10\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:99:\"/Users/tylerbarnes/Google Drive/Web Design/Projects/Rockware/development/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://rockware:8888/wp-snapshots/\";s:8:\"ScanFile\";s:57:\"20160329_rockware_56fb07ac1a31d7793160329225436_scan.json\";s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";N;s:7:\"Archive\";O:11:\"DUP_Archive\":13:{s:10:\"FilterDirs\";s:0:\"\";s:10:\"FilterExts\";s:0:\"\";s:13:\"FilterDirsAll\";a:0:{}s:13:\"FilterExtsAll\";a:0:{}s:8:\"FilterOn\";i:0;s:4:\"File\";N;s:6:\"Format\";s:3:\"ZIP\";s:7:\"PackDir\";s:82:\"/Users/tylerbarnes/Google Drive/Web Design/Projects/Rockware/development/wordpress\";s:4:\"Size\";i:0;s:4:\"Dirs\";a:0:{}s:5:\"Files\";a:0:{}s:10:\"FilterInfo\";O:23:\"DUP_Archive_Filter_Info\":6:{s:4:\"Dirs\";O:34:\"DUP_Archive_Filter_Scope_Directory\":4:{s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:5:\"Files\";O:29:\"DUP_Archive_Filter_Scope_File\":5:{s:4:\"Size\";a:0:{}s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:4:\"Exts\";O:29:\"DUP_Archive_Filter_Scope_Base\":2:{s:4:\"Core\";a:0:{}s:8:\"Instance\";a:0:{}}s:9:\"UDirCount\";i:0;s:10:\"UFileCount\";i:0;s:9:\"UExtCount\";i:0;}s:10:\"\0*\0Package\";O:11:\"DUP_Package\":21:{s:2:\"ID\";N;s:4:\"Name\";s:17:\"20160329_rockware\";s:4:\"Hash\";s:29:\"56fb07ac1a31d7793160329225436\";s:8:\"NameHash\";s:47:\"20160329_rockware_56fb07ac1a31d7793160329225436\";s:7:\"Version\";s:5:\"1.1.6\";s:9:\"VersionWP\";s:5:\"4.4.2\";s:9:\"VersionDB\";s:6:\"5.5.42\";s:10:\"VersionPHP\";s:6:\"5.6.10\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:99:\"/Users/tylerbarnes/Google Drive/Web Design/Projects/Rockware/development/wordpress/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://rockware:8888/wp-snapshots/\";s:8:\"ScanFile\";N;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";N;s:6:\"WPUser\";N;s:7:\"Archive\";r:20;s:9:\"Installer\";O:13:\"DUP_Installer\":12:{s:4:\"File\";N;s:4:\"Size\";i:0;s:10:\"OptsDBHost\";s:0:\"\";s:10:\"OptsDBPort\";s:0:\"\";s:10:\"OptsDBName\";s:0:\"\";s:10:\"OptsDBUser\";s:0:\"\";s:12:\"OptsSSLAdmin\";i:0;s:12:\"OptsSSLLogin\";i:0;s:11:\"OptsCacheWP\";i:0;s:13:\"OptsCachePath\";i:0;s:10:\"OptsURLNew\";s:0:\"\";s:10:\"\0*\0Package\";r:50;}s:8:\"Database\";O:12:\"DUP_Database\":12:{s:4:\"Type\";s:5:\"MySQL\";s:4:\"Size\";N;s:4:\"File\";N;s:4:\"Path\";N;s:12:\"FilterTables\";s:0:\"\";s:8:\"FilterOn\";i:0;s:4:\"Name\";N;s:10:\"Compatible\";s:0:\"\";s:10:\"\0*\0Package\";r:50;s:25:\"\0DUP_Database\0dbStorePath\";N;s:23:\"\0DUP_Database\0EOFMarker\";s:0:\"\";s:26:\"\0DUP_Database\0networkFlush\";b:0;}}}s:9:\"Installer\";r:70;s:8:\"Database\";r:83;}", "yes");

/* INSERT TABLE DATA: wp_postmeta */
INSERT INTO `wp_postmeta` VALUES("1", "2", "_wp_page_template", "default");
INSERT INTO `wp_postmeta` VALUES("2", "2", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("3", "2", "_wp_trash_meta_time", "1458602570");
INSERT INTO `wp_postmeta` VALUES("4", "5", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("5", "5", "_edit_lock", "1459220915:1");
INSERT INTO `wp_postmeta` VALUES("6", "7", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("7", "7", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("8", "7", "_menu_item_object_id", "5");
INSERT INTO `wp_postmeta` VALUES("9", "7", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("10", "7", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("11", "7", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("12", "7", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("13", "7", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("14", "8", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("15", "9", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("16", "9", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("17", "9", "_menu_item_object_id", "8");
INSERT INTO `wp_postmeta` VALUES("18", "9", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("19", "9", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("20", "9", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("21", "9", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("22", "9", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("23", "8", "_edit_lock", "1459221591:1");
INSERT INTO `wp_postmeta` VALUES("24", "11", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("25", "11", "_edit_lock", "1458672974:1");
INSERT INTO `wp_postmeta` VALUES("34", "14", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("35", "14", "_edit_lock", "1459217988:1");
INSERT INTO `wp_postmeta` VALUES("36", "16", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("37", "16", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("38", "16", "_menu_item_object_id", "14");
INSERT INTO `wp_postmeta` VALUES("39", "16", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("40", "16", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("41", "16", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("42", "16", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("43", "16", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("44", "17", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("45", "18", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("46", "18", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("47", "18", "_menu_item_object_id", "17");
INSERT INTO `wp_postmeta` VALUES("48", "18", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("49", "18", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("50", "18", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("51", "18", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("52", "18", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("53", "17", "_edit_lock", "1458685620:1");
INSERT INTO `wp_postmeta` VALUES("54", "20", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("55", "21", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("56", "21", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("57", "21", "_menu_item_object_id", "20");
INSERT INTO `wp_postmeta` VALUES("58", "21", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("59", "21", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("60", "21", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("61", "21", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("62", "21", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("63", "20", "_edit_lock", "1459217990:1");
INSERT INTO `wp_postmeta` VALUES("64", "24", "_wp_attached_file", "2016/03/logo.png");
INSERT INTO `wp_postmeta` VALUES("65", "24", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:314;s:6:\"height\";i:96;s:4:\"file\";s:16:\"2016/03/logo.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-150x96.png\";s:5:\"width\";i:150;s:6:\"height\";i:96;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x92.png\";s:5:\"width\";i:300;s:6:\"height\";i:92;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("66", "8", "_wp_page_template", "page-templates/uberuns.php");
INSERT INTO `wp_postmeta` VALUES("67", "11", "_wp_page_template", "page-templates/unsereleistungen.php");
INSERT INTO `wp_postmeta` VALUES("68", "11", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("69", "11", "_wp_trash_meta_time", "1458681029");
INSERT INTO `wp_postmeta` VALUES("70", "14", "_wp_page_template", "page-templates/blog.php");
INSERT INTO `wp_postmeta` VALUES("71", "17", "_wp_page_template", "page-templates/referenzen.php");
INSERT INTO `wp_postmeta` VALUES("72", "20", "_wp_page_template", "page-templates/kontakt.php");
INSERT INTO `wp_postmeta` VALUES("73", "31", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("74", "31", "_edit_lock", "1459221594:1");
INSERT INTO `wp_postmeta` VALUES("75", "33", "_wp_attached_file", "2016/03/banner.jpg");
INSERT INTO `wp_postmeta` VALUES("76", "33", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:652;s:4:\"file\";s:18:\"2016/03/banner.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"banner-300x102.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"banner-768x261.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:261;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"banner-1024x348.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:348;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("77", "5", "_wp_page_template", "default");
INSERT INTO `wp_postmeta` VALUES("78", "34", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("79", "34", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("80", "5", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("81", "5", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("82", "35", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("83", "35", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("84", "8", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("85", "8", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("86", "36", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("87", "36", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("88", "17", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("89", "17", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("90", "37", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("91", "37", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("92", "20", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("93", "20", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("94", "38", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("95", "38", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("96", "14", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("97", "14", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("98", "39", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("99", "39", "_edit_lock", "1459029448:1");
INSERT INTO `wp_postmeta` VALUES("100", "43", "_wp_attached_file", "2016/03/cq5_day_adobe_cms_logo.png");
INSERT INTO `wp_postmeta` VALUES("101", "43", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:150;s:6:\"height\";i:65;s:4:\"file\";s:34:\"2016/03/cq5_day_adobe_cms_logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"cq5_day_adobe_cms_logo-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("102", "44", "_wp_attached_file", "2016/03/crx-logo.png");
INSERT INTO `wp_postmeta` VALUES("103", "44", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:150;s:6:\"height\";i:65;s:4:\"file\";s:20:\"2016/03/crx-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"crx-logo-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("104", "45", "_wp_attached_file", "2016/03/java-logo.png");
INSERT INTO `wp_postmeta` VALUES("105", "45", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:150;s:6:\"height\";i:65;s:4:\"file\";s:21:\"2016/03/java-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"java-logo-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("106", "46", "_wp_attached_file", "2016/03/magnolia-cms-logo.png");
INSERT INTO `wp_postmeta` VALUES("107", "46", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:150;s:6:\"height\";i:65;s:4:\"file\";s:29:\"2016/03/magnolia-cms-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"magnolia-cms-logo-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("108", "47", "_wp_attached_file", "2016/03/open-logo.png");
INSERT INTO `wp_postmeta` VALUES("109", "47", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:150;s:6:\"height\";i:65;s:4:\"file\";s:21:\"2016/03/open-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"open-logo-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("110", "48", "_wp_attached_file", "2016/03/xml-logo.png");
INSERT INTO `wp_postmeta` VALUES("111", "48", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:150;s:6:\"height\";i:65;s:4:\"file\";s:20:\"2016/03/xml-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"xml-logo-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("112", "49", "_wp_attached_file", "2016/03/xslt-logo.png");
INSERT INTO `wp_postmeta` VALUES("113", "49", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:150;s:6:\"height\";i:65;s:4:\"file\";s:21:\"2016/03/xslt-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"xslt-logo-150x65.png\";s:5:\"width\";i:150;s:6:\"height\";i:65;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("114", "50", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("115", "50", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("116", "50", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("117", "50", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("118", "50", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("119", "50", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("120", "50", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("121", "50", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("122", "50", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("123", "50", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("124", "50", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("125", "50", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("126", "50", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("127", "50", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("128", "50", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("129", "50", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("130", "50", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("131", "50", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("132", "50", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("133", "50", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("134", "50", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("135", "50", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("136", "50", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("137", "50", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("138", "50", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("139", "50", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("140", "50", "past_clients_6_past_client_icons", "49");
INSERT INTO `wp_postmeta` VALUES("141", "50", "_past_clients_6_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("142", "50", "past_clients_6_past_client_icon_alt", "xlst");
INSERT INTO `wp_postmeta` VALUES("143", "50", "_past_clients_6_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("144", "50", "past_clients", "7");
INSERT INTO `wp_postmeta` VALUES("145", "50", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("146", "5", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("147", "5", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("148", "5", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("149", "5", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("150", "5", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("151", "5", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("152", "5", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("153", "5", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("154", "5", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("155", "5", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("156", "5", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("157", "5", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("158", "5", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("159", "5", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("160", "5", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("161", "5", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("162", "5", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("163", "5", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("164", "5", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("165", "5", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("166", "5", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("167", "5", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("168", "5", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("169", "5", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("174", "5", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("175", "5", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("176", "51", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("177", "51", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("178", "51", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("179", "51", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("180", "51", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("181", "51", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("182", "51", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("183", "51", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("184", "51", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("185", "51", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("186", "51", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("187", "51", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("188", "51", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("189", "51", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("190", "51", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("191", "51", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("192", "51", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("193", "51", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("194", "51", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("195", "51", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("196", "51", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("197", "51", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("198", "51", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("199", "51", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("200", "51", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("201", "51", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("202", "51", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("203", "51", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("204", "52", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("205", "52", "_edit_lock", "1459218102:1");
INSERT INTO `wp_postmeta` VALUES("206", "55", "_wp_attached_file", "2016/03/iphone-pick.png");
INSERT INTO `wp_postmeta` VALUES("207", "55", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:561;s:6:\"height\";i:374;s:4:\"file\";s:23:\"2016/03/iphone-pick.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"iphone-pick-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"iphone-pick-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("208", "56", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("209", "56", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("210", "56", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("211", "56", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("212", "56", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("213", "56", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("214", "56", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("215", "56", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("216", "56", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("217", "56", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("218", "56", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("219", "56", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("220", "56", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("221", "56", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("222", "56", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("223", "56", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("224", "56", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("225", "56", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("226", "56", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("227", "56", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("228", "56", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("229", "56", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("230", "56", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("231", "56", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("232", "56", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("233", "56", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("234", "56", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("235", "56", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("236", "56", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("237", "56", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("238", "56", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("239", "56", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("240", "5", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("241", "5", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("242", "5", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("243", "5", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("244", "5", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("245", "5", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("246", "58", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("247", "58", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("248", "58", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("249", "58", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("250", "58", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("251", "58", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("252", "58", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("253", "58", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("254", "58", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("255", "58", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("256", "58", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("257", "58", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("258", "58", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("259", "58", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("260", "58", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("261", "58", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("262", "58", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("263", "58", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("264", "58", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("265", "58", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("266", "58", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("267", "58", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("268", "58", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("269", "58", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("270", "58", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("271", "58", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("272", "58", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("273", "58", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("274", "58", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("275", "58", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("276", "58", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("277", "58", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("278", "58", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("279", "58", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("280", "61", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("281", "61", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("282", "61", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("283", "61", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("284", "61", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("285", "61", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("286", "61", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("287", "61", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("288", "61", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("289", "61", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("290", "61", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("291", "61", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("292", "61", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("293", "61", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("294", "61", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("295", "61", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("296", "61", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("297", "61", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("298", "61", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("299", "61", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("300", "61", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("301", "61", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("302", "61", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("303", "61", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("304", "61", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("305", "61", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("306", "61", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("307", "61", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("308", "61", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("309", "61", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("310", "61", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("311", "61", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("312", "61", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("313", "61", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("314", "61", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("315", "61", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("316", "61", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("317", "61", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("318", "5", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("319", "5", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("320", "5", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("321", "5", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("322", "62", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("323", "62", "_edit_lock", "1459218057:1");
INSERT INTO `wp_postmeta` VALUES("324", "64", "_wp_attached_file", "2016/03/city.jpg");
INSERT INTO `wp_postmeta` VALUES("325", "64", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:570;s:4:\"file\";s:16:\"2016/03/city.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"city-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"city-300x89.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:89;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"city-768x228.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:228;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"city-1024x304.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:304;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("326", "65", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("327", "65", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("328", "65", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("329", "65", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("330", "65", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("331", "65", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("332", "65", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("333", "65", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("334", "65", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("335", "65", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("336", "65", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("337", "65", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("338", "65", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("339", "65", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("340", "65", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("341", "65", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("342", "65", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("343", "65", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("344", "65", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("345", "65", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("346", "65", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("347", "65", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("348", "65", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("349", "65", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("350", "65", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("351", "65", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("352", "65", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("353", "65", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("354", "65", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("355", "65", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("356", "65", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("357", "65", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("358", "65", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("359", "65", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("360", "65", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("361", "65", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("362", "65", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("363", "65", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("364", "65", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("365", "65", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("366", "5", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("367", "5", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("368", "66", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("369", "66", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("370", "66", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("371", "66", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("372", "66", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("373", "66", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("374", "66", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("375", "66", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("376", "66", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("377", "66", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("378", "66", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("379", "66", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("380", "66", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("381", "66", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("382", "66", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("383", "66", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("384", "66", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("385", "66", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("386", "66", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("387", "66", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("388", "66", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("389", "66", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("390", "66", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("391", "66", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("392", "66", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("393", "66", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("394", "66", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("395", "66", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("396", "66", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("397", "66", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("398", "66", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("399", "66", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("400", "66", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("401", "66", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("402", "66", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("403", "66", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("404", "66", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("405", "66", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("406", "66", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("407", "66", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("408", "71", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("409", "71", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("410", "71", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("411", "71", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("412", "71", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("413", "71", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("414", "71", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("415", "71", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("416", "71", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("417", "71", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("418", "71", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("419", "71", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("420", "71", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("421", "71", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("422", "71", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("423", "71", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("424", "71", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("425", "71", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("426", "71", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("427", "71", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("428", "71", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("429", "71", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("430", "71", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("431", "71", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("432", "71", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("433", "71", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("434", "71", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("435", "71", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("436", "71", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("437", "71", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("438", "71", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("439", "71", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("440", "71", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("441", "71", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("442", "71", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("443", "71", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("444", "71", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("445", "71", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("446", "71", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("447", "71", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("448", "71", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("449", "71", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("450", "71", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("451", "71", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("452", "71", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("453", "71", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("454", "5", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("455", "5", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("456", "5", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("457", "5", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("458", "5", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("459", "5", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("460", "72", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("461", "72", "_edit_lock", "1459218992:1");
INSERT INTO `wp_postmeta` VALUES("462", "76", "_wp_attached_file", "2016/03/people.jpg");
INSERT INTO `wp_postmeta` VALUES("463", "76", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:624;s:6:\"height\";i:303;s:4:\"file\";s:18:\"2016/03/people.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"people-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"people-300x146.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:146;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("464", "77", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("465", "77", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("466", "77", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("467", "77", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("468", "77", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("469", "77", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("470", "77", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("471", "77", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("472", "77", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("473", "77", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("474", "77", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("475", "77", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("476", "77", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("477", "77", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("478", "77", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("479", "77", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("480", "77", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("481", "77", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("482", "77", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("483", "77", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("484", "77", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("485", "77", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("486", "77", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("487", "77", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("488", "77", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("489", "77", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("490", "77", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("491", "77", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("492", "77", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("493", "77", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("494", "77", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("495", "77", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("496", "77", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("497", "77", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("498", "77", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("499", "77", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("500", "77", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("501", "77", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("502", "77", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("503", "77", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("504", "77", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("505", "77", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("506", "77", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("507", "77", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("508", "77", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("509", "77", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("510", "77", "wir_title", "WIR <span>ROCKEN</span> IHRE WELT");
INSERT INTO `wp_postmeta` VALUES("511", "77", "_wir_title", "field_56f9e75dd54d9");
INSERT INTO `wp_postmeta` VALUES("512", "77", "wir_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `wp_postmeta` VALUES("513", "77", "_wir_paragraph", "field_56f9e796d54da");
INSERT INTO `wp_postmeta` VALUES("514", "77", "wir_image", "76");
INSERT INTO `wp_postmeta` VALUES("515", "77", "_wir_image", "field_56f9e7b5d54db");
INSERT INTO `wp_postmeta` VALUES("516", "5", "wir_title", "WIR <span>ROCKEN</span> IHRE WELT");
INSERT INTO `wp_postmeta` VALUES("517", "5", "_wir_title", "field_56f9e75dd54d9");
INSERT INTO `wp_postmeta` VALUES("518", "5", "wir_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `wp_postmeta` VALUES("519", "5", "_wir_paragraph", "field_56f9e796d54da");
INSERT INTO `wp_postmeta` VALUES("520", "5", "wir_image", "76");
INSERT INTO `wp_postmeta` VALUES("521", "5", "_wir_image", "field_56f9e7b5d54db");
INSERT INTO `wp_postmeta` VALUES("522", "80", "_edit_lock", "1459219369:1");
INSERT INTO `wp_postmeta` VALUES("523", "80", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("524", "84", "_wp_attached_file", "2016/03/bmw.png");
INSERT INTO `wp_postmeta` VALUES("525", "84", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:46;s:6:\"height\";i:52;s:4:\"file\";s:15:\"2016/03/bmw.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("526", "85", "_wp_attached_file", "2016/03/google.png");
INSERT INTO `wp_postmeta` VALUES("527", "85", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:115;s:6:\"height\";i:52;s:4:\"file\";s:18:\"2016/03/google.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("528", "86", "_wp_attached_file", "2016/03/ibm.png");
INSERT INTO `wp_postmeta` VALUES("529", "86", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:80;s:6:\"height\";i:52;s:4:\"file\";s:15:\"2016/03/ibm.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("530", "87", "_wp_attached_file", "2016/03/playboy.png");
INSERT INTO `wp_postmeta` VALUES("531", "87", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:129;s:6:\"height\";i:52;s:4:\"file\";s:19:\"2016/03/playboy.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("532", "88", "_wp_attached_file", "2016/03/siemens.png");
INSERT INTO `wp_postmeta` VALUES("533", "88", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:153;s:6:\"height\";i:52;s:4:\"file\";s:19:\"2016/03/siemens.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"siemens-150x52.png\";s:5:\"width\";i:150;s:6:\"height\";i:52;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("534", "89", "_wp_attached_file", "2016/03/zurich.png");
INSERT INTO `wp_postmeta` VALUES("535", "89", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:77;s:6:\"height\";i:52;s:4:\"file\";s:18:\"2016/03/zurich.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("536", "90", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("537", "90", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("538", "90", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("539", "90", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("540", "90", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("541", "90", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("542", "90", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("543", "90", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("544", "90", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("545", "90", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("546", "90", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("547", "90", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("548", "90", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("549", "90", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("550", "90", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("551", "90", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("552", "90", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("553", "90", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("554", "90", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("555", "90", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("556", "90", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("557", "90", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("558", "90", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("559", "90", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("560", "90", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("561", "90", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("562", "90", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("563", "90", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("564", "90", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("565", "90", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("566", "90", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("567", "90", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("568", "90", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("569", "90", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("570", "90", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("571", "90", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("572", "90", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("573", "90", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("574", "90", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("575", "90", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("576", "90", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("577", "90", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("578", "90", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("579", "90", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("580", "90", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("581", "90", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("582", "90", "wir_title", "WIR <span>ROCKEN</span> IHRE WELT");
INSERT INTO `wp_postmeta` VALUES("583", "90", "_wir_title", "field_56f9e75dd54d9");
INSERT INTO `wp_postmeta` VALUES("584", "90", "wir_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `wp_postmeta` VALUES("585", "90", "_wir_paragraph", "field_56f9e796d54da");
INSERT INTO `wp_postmeta` VALUES("586", "90", "wir_image", "76");
INSERT INTO `wp_postmeta` VALUES("587", "90", "_wir_image", "field_56f9e7b5d54db");
INSERT INTO `wp_postmeta` VALUES("588", "90", "wir_image_alt", "People");
INSERT INTO `wp_postmeta` VALUES("589", "90", "_wir_image_alt", "field_56f9ea35ff0ae");
INSERT INTO `wp_postmeta` VALUES("590", "90", "past_clients_2_0_past_client_icons_2", "84");
INSERT INTO `wp_postmeta` VALUES("591", "90", "_past_clients_2_0_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("592", "90", "past_clients_2_0_past_client_icon_alt_2", "BMW");
INSERT INTO `wp_postmeta` VALUES("593", "90", "_past_clients_2_0_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("594", "90", "past_clients_2_1_past_client_icons_2", "85");
INSERT INTO `wp_postmeta` VALUES("595", "90", "_past_clients_2_1_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("596", "90", "past_clients_2_1_past_client_icon_alt_2", "Google");
INSERT INTO `wp_postmeta` VALUES("597", "90", "_past_clients_2_1_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("598", "90", "past_clients_2_2_past_client_icons_2", "86");
INSERT INTO `wp_postmeta` VALUES("599", "90", "_past_clients_2_2_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("600", "90", "past_clients_2_2_past_client_icon_alt_2", "IBM");
INSERT INTO `wp_postmeta` VALUES("601", "90", "_past_clients_2_2_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("602", "90", "past_clients_2_3_past_client_icons_2", "87");
INSERT INTO `wp_postmeta` VALUES("603", "90", "_past_clients_2_3_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("604", "90", "past_clients_2_3_past_client_icon_alt_2", "Playboy");
INSERT INTO `wp_postmeta` VALUES("605", "90", "_past_clients_2_3_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("606", "90", "past_clients_2_4_past_client_icons_2", "88");
INSERT INTO `wp_postmeta` VALUES("607", "90", "_past_clients_2_4_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("608", "90", "past_clients_2_4_past_client_icon_alt_2", "Siemens");
INSERT INTO `wp_postmeta` VALUES("609", "90", "_past_clients_2_4_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("610", "90", "past_clients_2_5_past_client_icons_2", "89");
INSERT INTO `wp_postmeta` VALUES("611", "90", "_past_clients_2_5_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("612", "90", "past_clients_2_5_past_client_icon_alt_2", "Zurich");
INSERT INTO `wp_postmeta` VALUES("613", "90", "_past_clients_2_5_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("614", "90", "past_clients_2", "6");
INSERT INTO `wp_postmeta` VALUES("615", "90", "_past_clients_2", "field_56f9eace42cd3");
INSERT INTO `wp_postmeta` VALUES("616", "5", "wir_image_alt", "People");
INSERT INTO `wp_postmeta` VALUES("617", "5", "_wir_image_alt", "field_56f9ea35ff0ae");
INSERT INTO `wp_postmeta` VALUES("618", "5", "past_clients_2_0_past_client_icons_2", "84");
INSERT INTO `wp_postmeta` VALUES("619", "5", "_past_clients_2_0_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("620", "5", "past_clients_2_0_past_client_icon_alt_2", "BMW");
INSERT INTO `wp_postmeta` VALUES("621", "5", "_past_clients_2_0_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("622", "5", "past_clients_2_1_past_client_icons_2", "85");
INSERT INTO `wp_postmeta` VALUES("623", "5", "_past_clients_2_1_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("624", "5", "past_clients_2_1_past_client_icon_alt_2", "Google");
INSERT INTO `wp_postmeta` VALUES("625", "5", "_past_clients_2_1_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("626", "5", "past_clients_2_2_past_client_icons_2", "86");
INSERT INTO `wp_postmeta` VALUES("627", "5", "_past_clients_2_2_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("628", "5", "past_clients_2_2_past_client_icon_alt_2", "IBM");
INSERT INTO `wp_postmeta` VALUES("629", "5", "_past_clients_2_2_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("630", "5", "past_clients_2_3_past_client_icons_2", "87");
INSERT INTO `wp_postmeta` VALUES("631", "5", "_past_clients_2_3_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("632", "5", "past_clients_2_3_past_client_icon_alt_2", "Playboy");
INSERT INTO `wp_postmeta` VALUES("633", "5", "_past_clients_2_3_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("634", "5", "past_clients_2_4_past_client_icons_2", "88");
INSERT INTO `wp_postmeta` VALUES("635", "5", "_past_clients_2_4_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("636", "5", "past_clients_2_4_past_client_icon_alt_2", "Siemens");
INSERT INTO `wp_postmeta` VALUES("637", "5", "_past_clients_2_4_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("638", "5", "past_clients_2_5_past_client_icons_2", "89");
INSERT INTO `wp_postmeta` VALUES("639", "5", "_past_clients_2_5_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("640", "5", "past_clients_2_5_past_client_icon_alt_2", "Zurich");
INSERT INTO `wp_postmeta` VALUES("641", "5", "_past_clients_2_5_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("642", "5", "past_clients_2", "6");
INSERT INTO `wp_postmeta` VALUES("643", "5", "_past_clients_2", "field_56f9eace42cd3");
INSERT INTO `wp_postmeta` VALUES("644", "91", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("645", "91", "_edit_lock", "1459220150:1");
INSERT INTO `wp_postmeta` VALUES("646", "95", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("647", "95", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("648", "95", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("649", "95", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("650", "95", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("651", "95", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("652", "95", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("653", "95", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("654", "95", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("655", "95", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("656", "95", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("657", "95", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("658", "95", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("659", "95", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("660", "95", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("661", "95", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("662", "95", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("663", "95", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("664", "95", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("665", "95", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("666", "95", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("667", "95", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("668", "95", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("669", "95", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("670", "95", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("671", "95", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("672", "95", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("673", "95", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("674", "95", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("675", "95", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("676", "95", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("677", "95", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("678", "95", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("679", "95", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("680", "95", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("681", "95", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("682", "95", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("683", "95", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("684", "95", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("685", "95", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("686", "95", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("687", "95", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("688", "95", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("689", "95", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("690", "95", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("691", "95", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("692", "95", "wir_title", "WIR <span>ROCKEN</span> IHRE WELT");
INSERT INTO `wp_postmeta` VALUES("693", "95", "_wir_title", "field_56f9e75dd54d9");
INSERT INTO `wp_postmeta` VALUES("694", "95", "wir_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `wp_postmeta` VALUES("695", "95", "_wir_paragraph", "field_56f9e796d54da");
INSERT INTO `wp_postmeta` VALUES("696", "95", "wir_image", "76");
INSERT INTO `wp_postmeta` VALUES("697", "95", "_wir_image", "field_56f9e7b5d54db");
INSERT INTO `wp_postmeta` VALUES("698", "95", "wir_image_alt", "People");
INSERT INTO `wp_postmeta` VALUES("699", "95", "_wir_image_alt", "field_56f9ea35ff0ae");
INSERT INTO `wp_postmeta` VALUES("700", "95", "past_clients_2_0_past_client_icons_2", "84");
INSERT INTO `wp_postmeta` VALUES("701", "95", "_past_clients_2_0_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("702", "95", "past_clients_2_0_past_client_icon_alt_2", "BMW");
INSERT INTO `wp_postmeta` VALUES("703", "95", "_past_clients_2_0_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("704", "95", "past_clients_2_1_past_client_icons_2", "85");
INSERT INTO `wp_postmeta` VALUES("705", "95", "_past_clients_2_1_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("706", "95", "past_clients_2_1_past_client_icon_alt_2", "Google");
INSERT INTO `wp_postmeta` VALUES("707", "95", "_past_clients_2_1_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("708", "95", "past_clients_2_2_past_client_icons_2", "86");
INSERT INTO `wp_postmeta` VALUES("709", "95", "_past_clients_2_2_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("710", "95", "past_clients_2_2_past_client_icon_alt_2", "IBM");
INSERT INTO `wp_postmeta` VALUES("711", "95", "_past_clients_2_2_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("712", "95", "past_clients_2_3_past_client_icons_2", "87");
INSERT INTO `wp_postmeta` VALUES("713", "95", "_past_clients_2_3_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("714", "95", "past_clients_2_3_past_client_icon_alt_2", "Playboy");
INSERT INTO `wp_postmeta` VALUES("715", "95", "_past_clients_2_3_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("716", "95", "past_clients_2_4_past_client_icons_2", "88");
INSERT INTO `wp_postmeta` VALUES("717", "95", "_past_clients_2_4_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("718", "95", "past_clients_2_4_past_client_icon_alt_2", "Siemens");
INSERT INTO `wp_postmeta` VALUES("719", "95", "_past_clients_2_4_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("720", "95", "past_clients_2_5_past_client_icons_2", "89");
INSERT INTO `wp_postmeta` VALUES("721", "95", "_past_clients_2_5_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("722", "95", "past_clients_2_5_past_client_icon_alt_2", "Zurich");
INSERT INTO `wp_postmeta` VALUES("723", "95", "_past_clients_2_5_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("724", "95", "past_clients_2", "6");
INSERT INTO `wp_postmeta` VALUES("725", "95", "_past_clients_2", "field_56f9eace42cd3");
INSERT INTO `wp_postmeta` VALUES("726", "95", "contact_title_first_line", "LUST AUF EINEN");
INSERT INTO `wp_postmeta` VALUES("727", "95", "_contact_title_first_line", "field_56f9ec47e7da9");
INSERT INTO `wp_postmeta` VALUES("728", "95", "contact_title_second_line", "PRIVATEN GIG?");
INSERT INTO `wp_postmeta` VALUES("729", "95", "_contact_title_second_line", "field_56f9ec84e7daa");
INSERT INTO `wp_postmeta` VALUES("730", "95", "contact_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet.");
INSERT INTO `wp_postmeta` VALUES("731", "95", "_contact_paragraph", "field_56f9ece2e7dab");
INSERT INTO `wp_postmeta` VALUES("732", "5", "contact_title_first_line", "LUST AUF EINEN");
INSERT INTO `wp_postmeta` VALUES("733", "5", "_contact_title_first_line", "field_56f9ec47e7da9");
INSERT INTO `wp_postmeta` VALUES("734", "5", "contact_title_second_line", "PRIVATEN GIG?");
INSERT INTO `wp_postmeta` VALUES("735", "5", "_contact_title_second_line", "field_56f9ec84e7daa");
INSERT INTO `wp_postmeta` VALUES("736", "5", "contact_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet.");
INSERT INTO `wp_postmeta` VALUES("737", "5", "_contact_paragraph", "field_56f9ece2e7dab");
INSERT INTO `wp_postmeta` VALUES("738", "5", "contact_button_text", "ROCK IT");
INSERT INTO `wp_postmeta` VALUES("739", "5", "_contact_button_text", "field_56f9ed554cee6");
INSERT INTO `wp_postmeta` VALUES("740", "98", "_wp_attached_file", "2016/03/hands.png");
INSERT INTO `wp_postmeta` VALUES("741", "98", "_wp_attachment_metadata", "a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:611;s:4:\"file\";s:17:\"2016/03/hands.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"hands-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"hands-300x95.png\";s:5:\"width\";i:300;s:6:\"height\";i:95;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"hands-768x244.png\";s:5:\"width\";i:768;s:6:\"height\";i:244;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"hands-1024x326.png\";s:5:\"width\";i:1024;s:6:\"height\";i:326;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}");
INSERT INTO `wp_postmeta` VALUES("742", "99", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("743", "99", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("744", "99", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("745", "99", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("746", "99", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("747", "99", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("748", "99", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("749", "99", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("750", "99", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("751", "99", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("752", "99", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("753", "99", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("754", "99", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("755", "99", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("756", "99", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("757", "99", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("758", "99", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("759", "99", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("760", "99", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("761", "99", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("762", "99", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("763", "99", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("764", "99", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("765", "99", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("766", "99", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("767", "99", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("768", "99", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("769", "99", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("770", "99", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("771", "99", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("772", "99", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("773", "99", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("774", "99", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("775", "99", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("776", "99", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("777", "99", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("778", "99", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("779", "99", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("780", "99", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("781", "99", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("782", "99", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("783", "99", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("784", "99", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("785", "99", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("786", "99", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("787", "99", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("788", "99", "wir_title", "WIR <span>ROCKEN</span> IHRE WELT");
INSERT INTO `wp_postmeta` VALUES("789", "99", "_wir_title", "field_56f9e75dd54d9");
INSERT INTO `wp_postmeta` VALUES("790", "99", "wir_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `wp_postmeta` VALUES("791", "99", "_wir_paragraph", "field_56f9e796d54da");
INSERT INTO `wp_postmeta` VALUES("792", "99", "wir_image", "76");
INSERT INTO `wp_postmeta` VALUES("793", "99", "_wir_image", "field_56f9e7b5d54db");
INSERT INTO `wp_postmeta` VALUES("794", "99", "wir_image_alt", "People");
INSERT INTO `wp_postmeta` VALUES("795", "99", "_wir_image_alt", "field_56f9ea35ff0ae");
INSERT INTO `wp_postmeta` VALUES("796", "99", "past_clients_2_0_past_client_icons_2", "84");
INSERT INTO `wp_postmeta` VALUES("797", "99", "_past_clients_2_0_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("798", "99", "past_clients_2_0_past_client_icon_alt_2", "BMW");
INSERT INTO `wp_postmeta` VALUES("799", "99", "_past_clients_2_0_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("800", "99", "past_clients_2_1_past_client_icons_2", "85");
INSERT INTO `wp_postmeta` VALUES("801", "99", "_past_clients_2_1_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("802", "99", "past_clients_2_1_past_client_icon_alt_2", "Google");
INSERT INTO `wp_postmeta` VALUES("803", "99", "_past_clients_2_1_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("804", "99", "past_clients_2_2_past_client_icons_2", "86");
INSERT INTO `wp_postmeta` VALUES("805", "99", "_past_clients_2_2_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("806", "99", "past_clients_2_2_past_client_icon_alt_2", "IBM");
INSERT INTO `wp_postmeta` VALUES("807", "99", "_past_clients_2_2_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("808", "99", "past_clients_2_3_past_client_icons_2", "87");
INSERT INTO `wp_postmeta` VALUES("809", "99", "_past_clients_2_3_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("810", "99", "past_clients_2_3_past_client_icon_alt_2", "Playboy");
INSERT INTO `wp_postmeta` VALUES("811", "99", "_past_clients_2_3_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("812", "99", "past_clients_2_4_past_client_icons_2", "88");
INSERT INTO `wp_postmeta` VALUES("813", "99", "_past_clients_2_4_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("814", "99", "past_clients_2_4_past_client_icon_alt_2", "Siemens");
INSERT INTO `wp_postmeta` VALUES("815", "99", "_past_clients_2_4_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("816", "99", "past_clients_2_5_past_client_icons_2", "89");
INSERT INTO `wp_postmeta` VALUES("817", "99", "_past_clients_2_5_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("818", "99", "past_clients_2_5_past_client_icon_alt_2", "Zurich");
INSERT INTO `wp_postmeta` VALUES("819", "99", "_past_clients_2_5_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("820", "99", "past_clients_2", "6");
INSERT INTO `wp_postmeta` VALUES("821", "99", "_past_clients_2", "field_56f9eace42cd3");
INSERT INTO `wp_postmeta` VALUES("822", "99", "contact_background", "98");
INSERT INTO `wp_postmeta` VALUES("823", "99", "_contact_background", "field_56f9edd5d016a");
INSERT INTO `wp_postmeta` VALUES("824", "99", "contact_title_first_line", "LUST AUF EINEN");
INSERT INTO `wp_postmeta` VALUES("825", "99", "_contact_title_first_line", "field_56f9ec47e7da9");
INSERT INTO `wp_postmeta` VALUES("826", "99", "contact_title_second_line", "PRIVATEN GIG?");
INSERT INTO `wp_postmeta` VALUES("827", "99", "_contact_title_second_line", "field_56f9ec84e7daa");
INSERT INTO `wp_postmeta` VALUES("828", "99", "contact_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet.");
INSERT INTO `wp_postmeta` VALUES("829", "99", "_contact_paragraph", "field_56f9ece2e7dab");
INSERT INTO `wp_postmeta` VALUES("830", "99", "contact_button_text", "ROCK IT");
INSERT INTO `wp_postmeta` VALUES("831", "99", "_contact_button_text", "field_56f9ed554cee6");
INSERT INTO `wp_postmeta` VALUES("832", "5", "contact_background", "98");
INSERT INTO `wp_postmeta` VALUES("833", "5", "_contact_background", "field_56f9edd5d016a");
INSERT INTO `wp_postmeta` VALUES("834", "100", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("835", "100", "_edit_lock", "1459220939:1");
INSERT INTO `wp_postmeta` VALUES("836", "103", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("837", "103", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("838", "103", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("839", "103", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("840", "103", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("841", "103", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("842", "103", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("843", "103", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("844", "103", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("845", "103", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("846", "103", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("847", "103", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("848", "103", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("849", "103", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("850", "103", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("851", "103", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("852", "103", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("853", "103", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("854", "103", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("855", "103", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("856", "103", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("857", "103", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("858", "103", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("859", "103", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("860", "103", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("861", "103", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("862", "103", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("863", "103", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("864", "103", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("865", "103", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("866", "103", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("867", "103", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("868", "103", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("869", "103", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("870", "103", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("871", "103", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("872", "103", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("873", "103", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("874", "103", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("875", "103", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("876", "103", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("877", "103", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("878", "103", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("879", "103", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("880", "103", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("881", "103", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("882", "103", "wir_title", "WIR <span>ROCKEN</span> IHRE WELT");
INSERT INTO `wp_postmeta` VALUES("883", "103", "_wir_title", "field_56f9e75dd54d9");
INSERT INTO `wp_postmeta` VALUES("884", "103", "wir_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `wp_postmeta` VALUES("885", "103", "_wir_paragraph", "field_56f9e796d54da");
INSERT INTO `wp_postmeta` VALUES("886", "103", "wir_image", "76");
INSERT INTO `wp_postmeta` VALUES("887", "103", "_wir_image", "field_56f9e7b5d54db");
INSERT INTO `wp_postmeta` VALUES("888", "103", "wir_image_alt", "People");
INSERT INTO `wp_postmeta` VALUES("889", "103", "_wir_image_alt", "field_56f9ea35ff0ae");
INSERT INTO `wp_postmeta` VALUES("890", "103", "past_clients_2_0_past_client_icons_2", "84");
INSERT INTO `wp_postmeta` VALUES("891", "103", "_past_clients_2_0_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("892", "103", "past_clients_2_0_past_client_icon_alt_2", "BMW");
INSERT INTO `wp_postmeta` VALUES("893", "103", "_past_clients_2_0_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("894", "103", "past_clients_2_1_past_client_icons_2", "85");
INSERT INTO `wp_postmeta` VALUES("895", "103", "_past_clients_2_1_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("896", "103", "past_clients_2_1_past_client_icon_alt_2", "Google");
INSERT INTO `wp_postmeta` VALUES("897", "103", "_past_clients_2_1_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("898", "103", "past_clients_2_2_past_client_icons_2", "86");
INSERT INTO `wp_postmeta` VALUES("899", "103", "_past_clients_2_2_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("900", "103", "past_clients_2_2_past_client_icon_alt_2", "IBM");
INSERT INTO `wp_postmeta` VALUES("901", "103", "_past_clients_2_2_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("902", "103", "past_clients_2_3_past_client_icons_2", "87");
INSERT INTO `wp_postmeta` VALUES("903", "103", "_past_clients_2_3_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("904", "103", "past_clients_2_3_past_client_icon_alt_2", "Playboy");
INSERT INTO `wp_postmeta` VALUES("905", "103", "_past_clients_2_3_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("906", "103", "past_clients_2_4_past_client_icons_2", "88");
INSERT INTO `wp_postmeta` VALUES("907", "103", "_past_clients_2_4_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("908", "103", "past_clients_2_4_past_client_icon_alt_2", "Siemens");
INSERT INTO `wp_postmeta` VALUES("909", "103", "_past_clients_2_4_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("910", "103", "past_clients_2_5_past_client_icons_2", "89");
INSERT INTO `wp_postmeta` VALUES("911", "103", "_past_clients_2_5_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("912", "103", "past_clients_2_5_past_client_icon_alt_2", "Zurich");
INSERT INTO `wp_postmeta` VALUES("913", "103", "_past_clients_2_5_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("914", "103", "past_clients_2", "6");
INSERT INTO `wp_postmeta` VALUES("915", "103", "_past_clients_2", "field_56f9eace42cd3");
INSERT INTO `wp_postmeta` VALUES("916", "103", "contact_background", "98");
INSERT INTO `wp_postmeta` VALUES("917", "103", "_contact_background", "field_56f9edd5d016a");
INSERT INTO `wp_postmeta` VALUES("918", "103", "contact_title_first_line", "LUST AUF EINEN");
INSERT INTO `wp_postmeta` VALUES("919", "103", "_contact_title_first_line", "field_56f9ec47e7da9");
INSERT INTO `wp_postmeta` VALUES("920", "103", "contact_title_second_line", "PRIVATEN GIG?");
INSERT INTO `wp_postmeta` VALUES("921", "103", "_contact_title_second_line", "field_56f9ec84e7daa");
INSERT INTO `wp_postmeta` VALUES("922", "103", "contact_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet.");
INSERT INTO `wp_postmeta` VALUES("923", "103", "_contact_paragraph", "field_56f9ece2e7dab");
INSERT INTO `wp_postmeta` VALUES("924", "103", "contact_button_text", "ROCK IT");
INSERT INTO `wp_postmeta` VALUES("925", "103", "_contact_button_text", "field_56f9ed554cee6");
INSERT INTO `wp_postmeta` VALUES("926", "103", "subf_first_paragraph", "Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.");
INSERT INTO `wp_postmeta` VALUES("927", "103", "_subf_first_paragraph", "field_56f9ef5e8eafd");
INSERT INTO `wp_postmeta` VALUES("928", "103", "subf_second_paragraph", "Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. <br><br> 					Donec id elit non mi porta gravida at eget metus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.");
INSERT INTO `wp_postmeta` VALUES("929", "103", "_subf_second_paragraph", "field_56f9ef8f8eafe");
INSERT INTO `wp_postmeta` VALUES("930", "5", "subf_first_paragraph", "Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.");
INSERT INTO `wp_postmeta` VALUES("931", "5", "_subf_first_paragraph", "field_56f9ef5e8eafd");
INSERT INTO `wp_postmeta` VALUES("932", "5", "subf_second_paragraph", "Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.\r\n\r\nDonec id elit non mi porta gravida at eget metus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.");
INSERT INTO `wp_postmeta` VALUES("933", "5", "_subf_second_paragraph", "field_56f9ef8f8eafe");
INSERT INTO `wp_postmeta` VALUES("934", "104", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("935", "104", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("936", "104", "past_clients_0_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("937", "104", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("938", "104", "past_clients_0_past_client_icon_alt", "cq5");
INSERT INTO `wp_postmeta` VALUES("939", "104", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("940", "104", "past_clients_1_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("941", "104", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("942", "104", "past_clients_1_past_client_icon_alt", "crx");
INSERT INTO `wp_postmeta` VALUES("943", "104", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("944", "104", "past_clients_2_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("945", "104", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("946", "104", "past_clients_2_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("947", "104", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("948", "104", "past_clients_3_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("949", "104", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("950", "104", "past_clients_3_past_client_icon_alt", "magnolia");
INSERT INTO `wp_postmeta` VALUES("951", "104", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("952", "104", "past_clients_4_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("953", "104", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("954", "104", "past_clients_4_past_client_icon_alt", "open immo");
INSERT INTO `wp_postmeta` VALUES("955", "104", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("956", "104", "past_clients_5_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("957", "104", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("958", "104", "past_clients_5_past_client_icon_alt", "xml");
INSERT INTO `wp_postmeta` VALUES("959", "104", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("960", "104", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("961", "104", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("962", "104", "was_wir_leisten_title", "WAS WIR LEISTEN");
INSERT INTO `wp_postmeta` VALUES("963", "104", "_was_wir_leisten_title", "field_56f70c2657e95");
INSERT INTO `wp_postmeta` VALUES("964", "104", "was_wir_leisten_paragraph", "Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.");
INSERT INTO `wp_postmeta` VALUES("965", "104", "_was_wir_leisten_paragraph", "field_56f70dd7b85e4");
INSERT INTO `wp_postmeta` VALUES("966", "104", "was_wir_leisten_button", "DAS BIETEN WIR IHNEN");
INSERT INTO `wp_postmeta` VALUES("967", "104", "_was_wir_leisten_button", "field_56f70ec8b1031");
INSERT INTO `wp_postmeta` VALUES("968", "104", "was_wir_leisten_image", "55");
INSERT INTO `wp_postmeta` VALUES("969", "104", "_was_wir_leisten_image", "field_56f7067cfa67b");
INSERT INTO `wp_postmeta` VALUES("970", "104", "was_wir_leisten_image_alt", "Iphone and guitar pick");
INSERT INTO `wp_postmeta` VALUES("971", "104", "_was_wir_leisten_image_alt", "field_56f706f1fa67c");
INSERT INTO `wp_postmeta` VALUES("972", "104", "mit_liebe_background", "64");
INSERT INTO `wp_postmeta` VALUES("973", "104", "_mit_liebe_background", "field_56f83519c4d4e");
INSERT INTO `wp_postmeta` VALUES("974", "104", "mit_liebe_title", "MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS");
INSERT INTO `wp_postmeta` VALUES("975", "104", "_mit_liebe_title", "field_56f9da117ae65");
INSERT INTO `wp_postmeta` VALUES("976", "104", "mit_liebe_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");
INSERT INTO `wp_postmeta` VALUES("977", "104", "_mit_liebe_paragraph", "field_56f9da3d7ae66");
INSERT INTO `wp_postmeta` VALUES("978", "104", "mit_liebe_button_text", "MEHR ÜBER UNS");
INSERT INTO `wp_postmeta` VALUES("979", "104", "_mit_liebe_button_text", "field_56f9da587ae67");
INSERT INTO `wp_postmeta` VALUES("980", "104", "wir_title", "WIR <span>ROCKEN</span> IHRE WELT");
INSERT INTO `wp_postmeta` VALUES("981", "104", "_wir_title", "field_56f9e75dd54d9");
INSERT INTO `wp_postmeta` VALUES("982", "104", "wir_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `wp_postmeta` VALUES("983", "104", "_wir_paragraph", "field_56f9e796d54da");
INSERT INTO `wp_postmeta` VALUES("984", "104", "wir_image", "76");
INSERT INTO `wp_postmeta` VALUES("985", "104", "_wir_image", "field_56f9e7b5d54db");
INSERT INTO `wp_postmeta` VALUES("986", "104", "wir_image_alt", "People");
INSERT INTO `wp_postmeta` VALUES("987", "104", "_wir_image_alt", "field_56f9ea35ff0ae");
INSERT INTO `wp_postmeta` VALUES("988", "104", "past_clients_2_0_past_client_icons_2", "84");
INSERT INTO `wp_postmeta` VALUES("989", "104", "_past_clients_2_0_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("990", "104", "past_clients_2_0_past_client_icon_alt_2", "BMW");
INSERT INTO `wp_postmeta` VALUES("991", "104", "_past_clients_2_0_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("992", "104", "past_clients_2_1_past_client_icons_2", "85");
INSERT INTO `wp_postmeta` VALUES("993", "104", "_past_clients_2_1_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("994", "104", "past_clients_2_1_past_client_icon_alt_2", "Google");
INSERT INTO `wp_postmeta` VALUES("995", "104", "_past_clients_2_1_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("996", "104", "past_clients_2_2_past_client_icons_2", "86");
INSERT INTO `wp_postmeta` VALUES("997", "104", "_past_clients_2_2_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("998", "104", "past_clients_2_2_past_client_icon_alt_2", "IBM");
INSERT INTO `wp_postmeta` VALUES("999", "104", "_past_clients_2_2_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("1000", "104", "past_clients_2_3_past_client_icons_2", "87");
INSERT INTO `wp_postmeta` VALUES("1001", "104", "_past_clients_2_3_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("1002", "104", "past_clients_2_3_past_client_icon_alt_2", "Playboy");
INSERT INTO `wp_postmeta` VALUES("1003", "104", "_past_clients_2_3_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("1004", "104", "past_clients_2_4_past_client_icons_2", "88");
INSERT INTO `wp_postmeta` VALUES("1005", "104", "_past_clients_2_4_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("1006", "104", "past_clients_2_4_past_client_icon_alt_2", "Siemens");
INSERT INTO `wp_postmeta` VALUES("1007", "104", "_past_clients_2_4_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("1008", "104", "past_clients_2_5_past_client_icons_2", "89");
INSERT INTO `wp_postmeta` VALUES("1009", "104", "_past_clients_2_5_past_client_icons_2", "field_56f9eace45db0");
INSERT INTO `wp_postmeta` VALUES("1010", "104", "past_clients_2_5_past_client_icon_alt_2", "Zurich");
INSERT INTO `wp_postmeta` VALUES("1011", "104", "_past_clients_2_5_past_client_icon_alt_2", "field_56f9eace45dda");
INSERT INTO `wp_postmeta` VALUES("1012", "104", "past_clients_2", "6");
INSERT INTO `wp_postmeta` VALUES("1013", "104", "_past_clients_2", "field_56f9eace42cd3");
INSERT INTO `wp_postmeta` VALUES("1014", "104", "contact_background", "98");
INSERT INTO `wp_postmeta` VALUES("1015", "104", "_contact_background", "field_56f9edd5d016a");
INSERT INTO `wp_postmeta` VALUES("1016", "104", "contact_title_first_line", "LUST AUF EINEN");
INSERT INTO `wp_postmeta` VALUES("1017", "104", "_contact_title_first_line", "field_56f9ec47e7da9");
INSERT INTO `wp_postmeta` VALUES("1018", "104", "contact_title_second_line", "PRIVATEN GIG?");
INSERT INTO `wp_postmeta` VALUES("1019", "104", "_contact_title_second_line", "field_56f9ec84e7daa");
INSERT INTO `wp_postmeta` VALUES("1020", "104", "contact_paragraph", "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet.");
INSERT INTO `wp_postmeta` VALUES("1021", "104", "_contact_paragraph", "field_56f9ece2e7dab");
INSERT INTO `wp_postmeta` VALUES("1022", "104", "contact_button_text", "ROCK IT");
INSERT INTO `wp_postmeta` VALUES("1023", "104", "_contact_button_text", "field_56f9ed554cee6");
INSERT INTO `wp_postmeta` VALUES("1024", "104", "subf_first_paragraph", "Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.");
INSERT INTO `wp_postmeta` VALUES("1025", "104", "_subf_first_paragraph", "field_56f9ef5e8eafd");
INSERT INTO `wp_postmeta` VALUES("1026", "104", "subf_second_paragraph", "Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. <br><br>Donec id elit non mi porta gravida at eget metus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.");
INSERT INTO `wp_postmeta` VALUES("1027", "104", "_subf_second_paragraph", "field_56f9ef8f8eafe");
INSERT INTO `wp_postmeta` VALUES("1028", "106", "_edit_last", "1");
INSERT INTO `wp_postmeta` VALUES("1029", "106", "_edit_lock", "1459221296:1");
INSERT INTO `wp_postmeta` VALUES("1030", "109", "header_background_image", "33");
INSERT INTO `wp_postmeta` VALUES("1031", "109", "_header_background_image", "field_56f9f279f1ee0");
INSERT INTO `wp_postmeta` VALUES("1032", "109", "header_title", "WER WIR SIND");
INSERT INTO `wp_postmeta` VALUES("1033", "109", "_header_title", "field_56f9f294f1ee1");
INSERT INTO `wp_postmeta` VALUES("1034", "109", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("1035", "109", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("1036", "109", "past_clients_0_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("1037", "109", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1038", "109", "past_clients_0_past_client_icon_alt", "XML");
INSERT INTO `wp_postmeta` VALUES("1039", "109", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1040", "109", "past_clients_1_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("1041", "109", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1042", "109", "past_clients_1_past_client_icon_alt", "Open immo");
INSERT INTO `wp_postmeta` VALUES("1043", "109", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1044", "109", "past_clients_2_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("1045", "109", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1046", "109", "past_clients_2_past_client_icon_alt", "Magnolia");
INSERT INTO `wp_postmeta` VALUES("1047", "109", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1048", "109", "past_clients_3_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("1049", "109", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1050", "109", "past_clients_3_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("1051", "109", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1052", "109", "past_clients_4_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("1053", "109", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1054", "109", "past_clients_4_past_client_icon_alt", "CRX");
INSERT INTO `wp_postmeta` VALUES("1055", "109", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1056", "109", "past_clients_5_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("1057", "109", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1058", "109", "past_clients_5_past_client_icon_alt", "CQ5");
INSERT INTO `wp_postmeta` VALUES("1059", "109", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1060", "109", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("1061", "109", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("1062", "8", "header_background_image", "33");
INSERT INTO `wp_postmeta` VALUES("1063", "8", "_header_background_image", "field_56f9f279f1ee0");
INSERT INTO `wp_postmeta` VALUES("1064", "8", "header_title", "WER WIR SIND");
INSERT INTO `wp_postmeta` VALUES("1065", "8", "_header_title", "field_56f9f294f1ee1");
INSERT INTO `wp_postmeta` VALUES("1066", "8", "past_clients_0_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("1067", "8", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1068", "8", "past_clients_0_past_client_icon_alt", "XML");
INSERT INTO `wp_postmeta` VALUES("1069", "8", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1070", "8", "past_clients_1_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("1071", "8", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1072", "8", "past_clients_1_past_client_icon_alt", "Open immo");
INSERT INTO `wp_postmeta` VALUES("1073", "8", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1074", "8", "past_clients_2_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("1075", "8", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1076", "8", "past_clients_2_past_client_icon_alt", "Magnolia");
INSERT INTO `wp_postmeta` VALUES("1077", "8", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1078", "8", "past_clients_3_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("1079", "8", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1080", "8", "past_clients_3_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("1081", "8", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1082", "8", "past_clients_4_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("1083", "8", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1084", "8", "past_clients_4_past_client_icon_alt", "CRX");
INSERT INTO `wp_postmeta` VALUES("1085", "8", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1086", "8", "past_clients_5_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("1087", "8", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1088", "8", "past_clients_5_past_client_icon_alt", "CQ5");
INSERT INTO `wp_postmeta` VALUES("1089", "8", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1090", "8", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("1091", "8", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("1092", "110", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("1093", "110", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("1094", "110", "header_title", "WER WIR SIND");
INSERT INTO `wp_postmeta` VALUES("1095", "110", "_header_title", "field_56f9f294f1ee1");
INSERT INTO `wp_postmeta` VALUES("1096", "110", "past_clients_0_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("1097", "110", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1098", "110", "past_clients_0_past_client_icon_alt", "XML");
INSERT INTO `wp_postmeta` VALUES("1099", "110", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1100", "110", "past_clients_1_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("1101", "110", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1102", "110", "past_clients_1_past_client_icon_alt", "Open immo");
INSERT INTO `wp_postmeta` VALUES("1103", "110", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1104", "110", "past_clients_2_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("1105", "110", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1106", "110", "past_clients_2_past_client_icon_alt", "Magnolia");
INSERT INTO `wp_postmeta` VALUES("1107", "110", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1108", "110", "past_clients_3_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("1109", "110", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1110", "110", "past_clients_3_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("1111", "110", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1112", "110", "past_clients_4_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("1113", "110", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1114", "110", "past_clients_4_past_client_icon_alt", "CRX");
INSERT INTO `wp_postmeta` VALUES("1115", "110", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1116", "110", "past_clients_5_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("1117", "110", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1118", "110", "past_clients_5_past_client_icon_alt", "CQ5");
INSERT INTO `wp_postmeta` VALUES("1119", "110", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1120", "110", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("1121", "110", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("1122", "111", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("1123", "111", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("1124", "111", "header_title", "WER WIR SIND");
INSERT INTO `wp_postmeta` VALUES("1125", "111", "_header_title", "field_56f9f294f1ee1");
INSERT INTO `wp_postmeta` VALUES("1126", "111", "past_clients_0_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("1127", "111", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1128", "111", "past_clients_0_past_client_icon_alt", "XML");
INSERT INTO `wp_postmeta` VALUES("1129", "111", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1130", "111", "past_clients_1_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("1131", "111", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1132", "111", "past_clients_1_past_client_icon_alt", "Open immo");
INSERT INTO `wp_postmeta` VALUES("1133", "111", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1134", "111", "past_clients_2_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("1135", "111", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1136", "111", "past_clients_2_past_client_icon_alt", "Magnolia");
INSERT INTO `wp_postmeta` VALUES("1137", "111", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1138", "111", "past_clients_3_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("1139", "111", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1140", "111", "past_clients_3_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("1141", "111", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1142", "111", "past_clients_4_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("1143", "111", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1144", "111", "past_clients_4_past_client_icon_alt", "CRX");
INSERT INTO `wp_postmeta` VALUES("1145", "111", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1146", "111", "past_clients_5_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("1147", "111", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1148", "111", "past_clients_5_past_client_icon_alt", "CQ5");
INSERT INTO `wp_postmeta` VALUES("1149", "111", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1150", "111", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("1151", "111", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("1152", "112", "header_image", "");
INSERT INTO `wp_postmeta` VALUES("1153", "112", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("1154", "112", "header_title", "WER WIR SIND");
INSERT INTO `wp_postmeta` VALUES("1155", "112", "_header_title", "field_56f9f294f1ee1");
INSERT INTO `wp_postmeta` VALUES("1156", "112", "past_clients_0_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("1157", "112", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1158", "112", "past_clients_0_past_client_icon_alt", "XML");
INSERT INTO `wp_postmeta` VALUES("1159", "112", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1160", "112", "past_clients_1_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("1161", "112", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1162", "112", "past_clients_1_past_client_icon_alt", "Open immo");
INSERT INTO `wp_postmeta` VALUES("1163", "112", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1164", "112", "past_clients_2_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("1165", "112", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1166", "112", "past_clients_2_past_client_icon_alt", "Magnolia");
INSERT INTO `wp_postmeta` VALUES("1167", "112", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1168", "112", "past_clients_3_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("1169", "112", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1170", "112", "past_clients_3_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("1171", "112", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1172", "112", "past_clients_4_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("1173", "112", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1174", "112", "past_clients_4_past_client_icon_alt", "CRX");
INSERT INTO `wp_postmeta` VALUES("1175", "112", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1176", "112", "past_clients_5_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("1177", "112", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1178", "112", "past_clients_5_past_client_icon_alt", "CQ5");
INSERT INTO `wp_postmeta` VALUES("1179", "112", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1180", "112", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("1181", "112", "_past_clients", "field_56f700fa35c46");
INSERT INTO `wp_postmeta` VALUES("1182", "113", "header_image", "33");
INSERT INTO `wp_postmeta` VALUES("1183", "113", "_header_image", "field_56f1c0425ffe0");
INSERT INTO `wp_postmeta` VALUES("1184", "113", "header_title", "WER WIR SIND");
INSERT INTO `wp_postmeta` VALUES("1185", "113", "_header_title", "field_56f9f294f1ee1");
INSERT INTO `wp_postmeta` VALUES("1186", "113", "past_clients_0_past_client_icons", "48");
INSERT INTO `wp_postmeta` VALUES("1187", "113", "_past_clients_0_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1188", "113", "past_clients_0_past_client_icon_alt", "XML");
INSERT INTO `wp_postmeta` VALUES("1189", "113", "_past_clients_0_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1190", "113", "past_clients_1_past_client_icons", "47");
INSERT INTO `wp_postmeta` VALUES("1191", "113", "_past_clients_1_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1192", "113", "past_clients_1_past_client_icon_alt", "Open immo");
INSERT INTO `wp_postmeta` VALUES("1193", "113", "_past_clients_1_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1194", "113", "past_clients_2_past_client_icons", "46");
INSERT INTO `wp_postmeta` VALUES("1195", "113", "_past_clients_2_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1196", "113", "past_clients_2_past_client_icon_alt", "Magnolia");
INSERT INTO `wp_postmeta` VALUES("1197", "113", "_past_clients_2_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1198", "113", "past_clients_3_past_client_icons", "45");
INSERT INTO `wp_postmeta` VALUES("1199", "113", "_past_clients_3_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1200", "113", "past_clients_3_past_client_icon_alt", "java");
INSERT INTO `wp_postmeta` VALUES("1201", "113", "_past_clients_3_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1202", "113", "past_clients_4_past_client_icons", "44");
INSERT INTO `wp_postmeta` VALUES("1203", "113", "_past_clients_4_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1204", "113", "past_clients_4_past_client_icon_alt", "CRX");
INSERT INTO `wp_postmeta` VALUES("1205", "113", "_past_clients_4_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1206", "113", "past_clients_5_past_client_icons", "43");
INSERT INTO `wp_postmeta` VALUES("1207", "113", "_past_clients_5_past_client_icons", "field_56f7013a35c47");
INSERT INTO `wp_postmeta` VALUES("1208", "113", "past_clients_5_past_client_icon_alt", "CQ5");
INSERT INTO `wp_postmeta` VALUES("1209", "113", "_past_clients_5_past_client_icon_alt", "field_56f702fd90257");
INSERT INTO `wp_postmeta` VALUES("1210", "113", "past_clients", "6");
INSERT INTO `wp_postmeta` VALUES("1211", "113", "_past_clients", "field_56f700fa35c46");

/* INSERT TABLE DATA: wp_posts */
INSERT INTO `wp_posts` VALUES("1", "1", "2016-03-20 18:34:15", "2016-03-20 18:34:15", "Welcome to WordPress. This is your first post. Edit or delete it, then start writing!", "Hello world!", "", "publish", "open", "open", "", "hello-world", "", "", "2016-03-20 18:34:15", "2016-03-20 18:34:15", "", "0", "http://rockware:8888/?p=1", "0", "post", "", "1");
INSERT INTO `wp_posts` VALUES("2", "1", "2016-03-20 18:34:15", "2016-03-20 18:34:15", "This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://rockware:8888/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!", "Sample Page", "", "trash", "closed", "open", "", "sample-page", "", "", "2016-03-21 23:22:50", "2016-03-21 23:22:50", "", "0", "http://rockware:8888/?page_id=2", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("4", "1", "2016-03-21 23:22:50", "2016-03-21 23:22:50", "This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://rockware:8888/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!", "Sample Page", "", "inherit", "closed", "closed", "", "2-revision-v1", "", "", "2016-03-21 23:22:50", "2016-03-21 23:22:50", "", "2", "http://rockware:8888/2016/03/21/2-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("5", "1", "2016-03-21 23:29:12", "2016-03-21 23:29:12", "", "Start", "", "publish", "closed", "closed", "", "start", "", "", "2016-03-29 03:02:33", "2016-03-29 03:02:33", "", "0", "http://rockware:8888/?page_id=5", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("6", "1", "2016-03-21 23:22:57", "2016-03-21 23:22:57", "", "home", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-21 23:22:57", "2016-03-21 23:22:57", "", "5", "http://rockware:8888/2016/03/21/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("7", "1", "2016-03-21 23:29:12", "2016-03-21 23:29:12", " ", "", "", "publish", "closed", "closed", "", "7", "", "", "2016-03-22 21:12:26", "2016-03-22 21:12:26", "", "0", "http://rockware:8888/2016/03/21/7/", "1", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("8", "1", "2016-03-21 23:29:26", "2016-03-21 23:29:26", "", "Über uns", "", "publish", "closed", "closed", "", "uber-uns", "", "", "2016-03-29 03:20:13", "2016-03-29 03:20:13", "", "0", "http://rockware:8888/?page_id=8", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("9", "1", "2016-03-21 23:29:26", "2016-03-21 23:29:26", " ", "", "", "publish", "closed", "closed", "", "9", "", "", "2016-03-22 21:12:26", "2016-03-22 21:12:26", "", "0", "http://rockware:8888/2016/03/21/9/", "2", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("10", "1", "2016-03-21 23:29:26", "2016-03-21 23:29:26", "", "über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-21 23:29:26", "2016-03-21 23:29:26", "", "8", "http://rockware:8888/2016/03/21/8-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("11", "1", "2016-03-21 23:54:22", "2016-03-21 23:54:22", "", "unsere leistungen", "", "trash", "closed", "closed", "", "unsere-leistungen", "", "", "2016-03-22 21:10:29", "2016-03-22 21:10:29", "", "0", "http://rockware:8888/?page_id=11", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("13", "1", "2016-03-21 23:54:22", "2016-03-21 23:54:22", "", "unsere leistungen", "", "inherit", "closed", "closed", "", "11-revision-v1", "", "", "2016-03-21 23:54:22", "2016-03-21 23:54:22", "", "11", "http://rockware:8888/2016/03/21/11-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("14", "1", "2016-03-21 23:54:34", "2016-03-21 23:54:34", "", "Blog", "", "publish", "closed", "closed", "", "blog", "", "", "2016-03-22 22:09:37", "2016-03-22 22:09:37", "", "0", "http://rockware:8888/?page_id=14", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("15", "1", "2016-03-21 23:54:30", "2016-03-21 23:54:30", "", "blog", "", "inherit", "closed", "closed", "", "14-revision-v1", "", "", "2016-03-21 23:54:30", "2016-03-21 23:54:30", "", "14", "http://rockware:8888/2016/03/21/14-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("16", "1", "2016-03-21 23:54:34", "2016-03-21 23:54:34", " ", "", "", "publish", "closed", "closed", "", "16", "", "", "2016-03-22 21:12:26", "2016-03-22 21:12:26", "", "0", "http://rockware:8888/2016/03/21/16/", "3", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("17", "1", "2016-03-21 23:54:45", "2016-03-21 23:54:45", "", "Referenzen", "", "publish", "closed", "closed", "", "referenzen", "", "", "2016-03-22 22:09:21", "2016-03-22 22:09:21", "", "0", "http://rockware:8888/?page_id=17", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("18", "1", "2016-03-21 23:54:45", "2016-03-21 23:54:45", " ", "", "", "publish", "closed", "closed", "", "18", "", "", "2016-03-22 21:12:26", "2016-03-22 21:12:26", "", "0", "http://rockware:8888/2016/03/21/18/", "4", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("19", "1", "2016-03-21 23:54:45", "2016-03-21 23:54:45", "", "referenzen", "", "inherit", "closed", "closed", "", "17-revision-v1", "", "", "2016-03-21 23:54:45", "2016-03-21 23:54:45", "", "17", "http://rockware:8888/2016/03/21/17-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("20", "1", "2016-03-21 23:54:56", "2016-03-21 23:54:56", "", "Kontakt", "", "publish", "closed", "closed", "", "kontakt", "", "", "2016-03-22 22:09:29", "2016-03-22 22:09:29", "", "0", "http://rockware:8888/?page_id=20", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("21", "1", "2016-03-21 23:54:56", "2016-03-21 23:54:56", " ", "", "", "publish", "closed", "closed", "", "21", "", "", "2016-03-22 21:12:26", "2016-03-22 21:12:26", "", "0", "http://rockware:8888/2016/03/21/21/", "5", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("22", "1", "2016-03-21 23:54:56", "2016-03-21 23:54:56", "", "kontakt", "", "inherit", "closed", "closed", "", "20-revision-v1", "", "", "2016-03-21 23:54:56", "2016-03-21 23:54:56", "", "20", "http://rockware:8888/2016/03/21/20-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("23", "1", "2016-03-21 23:55:29", "2016-03-21 23:55:29", "", "start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-21 23:55:29", "2016-03-21 23:55:29", "", "5", "http://rockware:8888/2016/03/21/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("24", "1", "2016-03-22 00:06:44", "2016-03-22 00:06:44", "", "logo", "", "inherit", "open", "closed", "", "logo", "", "", "2016-03-22 00:06:44", "2016-03-22 00:06:44", "", "0", "http://rockware:8888/wp-content/uploads/2016/03/logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("25", "1", "2016-03-22 21:18:58", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2016-03-22 21:18:58", "0000-00-00 00:00:00", "", "0", "http://rockware:8888/?page_id=25", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("26", "1", "2016-03-22 21:25:22", "2016-03-22 21:25:22", "", "Blog", "", "inherit", "closed", "closed", "", "14-revision-v1", "", "", "2016-03-22 21:25:22", "2016-03-22 21:25:22", "", "14", "http://rockware:8888/2016/03/22/14-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("27", "1", "2016-03-22 21:25:26", "2016-03-22 21:25:26", "", "Kontakt", "", "inherit", "closed", "closed", "", "20-revision-v1", "", "", "2016-03-22 21:25:26", "2016-03-22 21:25:26", "", "20", "http://rockware:8888/2016/03/22/20-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("28", "1", "2016-03-22 21:25:31", "2016-03-22 21:25:31", "", "Referenzen", "", "inherit", "closed", "closed", "", "17-revision-v1", "", "", "2016-03-22 21:25:31", "2016-03-22 21:25:31", "", "17", "http://rockware:8888/2016/03/22/17-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("29", "1", "2016-03-22 21:25:41", "2016-03-22 21:25:41", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-22 21:25:41", "2016-03-22 21:25:41", "", "5", "http://rockware:8888/2016/03/22/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("30", "1", "2016-03-22 21:25:49", "2016-03-22 21:25:49", "", "Über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-22 21:25:49", "2016-03-22 21:25:49", "", "8", "http://rockware:8888/2016/03/22/8-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("31", "1", "2016-03-22 22:00:49", "2016-03-22 22:00:49", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"top_level\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Header background", "header-background", "publish", "closed", "closed", "", "group_56f1c0348747f", "", "", "2016-03-29 03:19:39", "2016-03-29 03:19:39", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=31", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("32", "1", "2016-03-22 22:00:49", "2016-03-22 22:00:49", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:30:\"Upload an image for the header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "Header Image", "header_image", "publish", "closed", "closed", "", "field_56f1c0425ffe0", "", "", "2016-03-29 03:19:39", "2016-03-29 03:19:39", "", "31", "http://rockware:8888/?post_type=acf-field&#038;p=32", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("33", "1", "2016-03-22 22:03:05", "2016-03-22 22:03:05", "", "banner", "", "inherit", "open", "closed", "", "banner", "", "", "2016-03-22 22:03:05", "2016-03-22 22:03:05", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/banner.jpg", "0", "attachment", "image/jpeg", "0");
INSERT INTO `wp_posts` VALUES("34", "1", "2016-03-22 22:03:17", "2016-03-22 22:03:17", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-22 22:03:17", "2016-03-22 22:03:17", "", "5", "http://rockware:8888/2016/03/22/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("35", "1", "2016-03-22 22:09:07", "2016-03-22 22:09:07", "", "Über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-22 22:09:07", "2016-03-22 22:09:07", "", "8", "http://rockware:8888/2016/03/22/8-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("36", "1", "2016-03-22 22:09:21", "2016-03-22 22:09:21", "", "Referenzen", "", "inherit", "closed", "closed", "", "17-revision-v1", "", "", "2016-03-22 22:09:21", "2016-03-22 22:09:21", "", "17", "http://rockware:8888/2016/03/22/17-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("37", "1", "2016-03-22 22:09:29", "2016-03-22 22:09:29", "", "Kontakt", "", "inherit", "closed", "closed", "", "20-revision-v1", "", "", "2016-03-22 22:09:29", "2016-03-22 22:09:29", "", "20", "http://rockware:8888/2016/03/22/20-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("38", "1", "2016-03-22 22:09:37", "2016-03-22 22:09:37", "", "Blog", "", "inherit", "closed", "closed", "", "14-revision-v1", "", "", "2016-03-22 22:09:37", "2016-03-22 22:09:37", "", "14", "http://rockware:8888/2016/03/22/14-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("39", "1", "2016-03-26 21:36:10", "2016-03-26 21:36:10", "a:7:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"5\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"8\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Past clients", "past-clients", "publish", "closed", "closed", "", "group_56f7006885a18", "", "", "2016-03-26 21:59:49", "2016-03-26 21:59:49", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=39", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("40", "1", "2016-03-26 21:38:54", "2016-03-26 21:38:54", "a:9:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:12:\"past clients\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:15:\"Add past client\";}", "Past Clients", "past_clients", "publish", "closed", "closed", "", "field_56f700fa35c46", "", "", "2016-03-26 21:59:49", "2016-03-26 21:59:49", "", "39", "http://rockware:8888/?post_type=acf-field&#038;p=40", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("41", "1", "2016-03-26 21:38:54", "2016-03-26 21:38:54", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:37:\"Add a grey logo for every past client\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "past client icons", "past_client_icons", "publish", "closed", "closed", "", "field_56f7013a35c47", "", "", "2016-03-26 21:47:01", "2016-03-26 21:47:01", "", "40", "http://rockware:8888/?post_type=acf-field&#038;p=41", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("42", "1", "2016-03-26 21:47:01", "2016-03-26 21:47:01", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:19:\"Add the client name\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "past client icon alt", "past_client_icon_alt", "publish", "closed", "closed", "", "field_56f702fd90257", "", "", "2016-03-26 21:47:01", "2016-03-26 21:47:01", "", "40", "http://rockware:8888/?post_type=acf-field&p=42", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("43", "1", "2016-03-26 21:51:52", "2016-03-26 21:51:52", "", "cq5_day_adobe_cms_logo", "", "inherit", "open", "closed", "", "cq5_day_adobe_cms_logo", "", "", "2016-03-26 21:51:52", "2016-03-26 21:51:52", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/cq5_day_adobe_cms_logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("44", "1", "2016-03-26 21:51:53", "2016-03-26 21:51:53", "", "crx-logo", "", "inherit", "open", "closed", "", "crx-logo", "", "", "2016-03-26 21:51:53", "2016-03-26 21:51:53", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/crx-logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("45", "1", "2016-03-26 21:51:55", "2016-03-26 21:51:55", "", "java-logo", "", "inherit", "open", "closed", "", "java-logo", "", "", "2016-03-26 21:51:55", "2016-03-26 21:51:55", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/java-logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("46", "1", "2016-03-26 21:51:56", "2016-03-26 21:51:56", "", "magnolia-cms-logo", "", "inherit", "open", "closed", "", "magnolia-cms-logo", "", "", "2016-03-26 21:51:56", "2016-03-26 21:51:56", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/magnolia-cms-logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("47", "1", "2016-03-26 21:51:57", "2016-03-26 21:51:57", "", "open-logo", "", "inherit", "open", "closed", "", "open-logo", "", "", "2016-03-26 21:51:57", "2016-03-26 21:51:57", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/open-logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("48", "1", "2016-03-26 21:51:58", "2016-03-26 21:51:58", "", "xml-logo", "", "inherit", "open", "closed", "", "xml-logo", "", "", "2016-03-26 21:51:58", "2016-03-26 21:51:58", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/xml-logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("49", "1", "2016-03-26 21:51:59", "2016-03-26 21:51:59", "", "xslt-logo", "", "inherit", "open", "closed", "", "xslt-logo", "", "", "2016-03-26 21:51:59", "2016-03-26 21:51:59", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/xslt-logo.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("50", "1", "2016-03-26 21:52:44", "2016-03-26 21:52:44", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-26 21:52:44", "2016-03-26 21:52:44", "", "5", "http://rockware:8888/2016/03/26/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("51", "1", "2016-03-26 21:55:27", "2016-03-26 21:55:27", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-26 21:55:27", "2016-03-26 21:55:27", "", "5", "http://rockware:8888/2016/03/26/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("52", "1", "2016-03-26 22:04:18", "2016-03-26 22:04:18", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"5\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Was wir leisten", "was-wir-leisten", "publish", "closed", "closed", "", "group_56f7065a41784", "", "", "2016-03-29 02:23:39", "2016-03-29 02:23:39", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=52", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("53", "1", "2016-03-26 22:04:18", "2016-03-26 22:04:18", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:43:\"Add an image for the was wir listen section\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";i:561;s:10:\"min_height\";i:374;s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";i:561;s:10:\"max_height\";i:374;s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "Image", "was_wir_leisten_image", "publish", "closed", "closed", "", "field_56f7067cfa67b", "", "", "2016-03-26 22:36:24", "2016-03-26 22:36:24", "", "52", "http://rockware:8888/?post_type=acf-field&#038;p=53", "3", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("54", "1", "2016-03-26 22:04:18", "2016-03-26 22:04:18", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:38:\"Write a short description of the image\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Image description", "was_wir_leisten_image_alt", "publish", "closed", "closed", "", "field_56f706f1fa67c", "", "", "2016-03-26 22:36:24", "2016-03-26 22:36:24", "", "52", "http://rockware:8888/?post_type=acf-field&#038;p=54", "4", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("55", "1", "2016-03-26 22:04:47", "2016-03-26 22:04:47", "", "iphone-pick", "", "inherit", "open", "closed", "", "iphone-pick", "", "", "2016-03-26 22:04:47", "2016-03-26 22:04:47", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/iphone-pick.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("56", "1", "2016-03-26 22:05:03", "2016-03-26 22:05:03", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-26 22:05:03", "2016-03-26 22:05:03", "", "5", "http://rockware:8888/2016/03/26/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("57", "1", "2016-03-26 22:26:15", "2016-03-26 22:26:15", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:45:\"Enter a title for the was wir leisten section\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:15:\"was wir leisten\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Title", "was_wir_leisten_title", "publish", "closed", "closed", "", "field_56f70c2657e95", "", "", "2016-03-26 22:33:01", "2016-03-26 22:33:01", "", "52", "http://rockware:8888/?post_type=acf-field&#038;p=57", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("58", "1", "2016-03-26 22:30:05", "2016-03-26 22:30:05", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-26 22:30:05", "2016-03-26 22:30:05", "", "5", "http://rockware:8888/2016/03/26/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("59", "1", "2016-03-26 22:33:01", "2016-03-26 22:33:01", "a:12:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:20:\"Enter paragraph text\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:7:\"wpautop\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Paragraph", "was_wir_leisten_paragraph", "publish", "closed", "closed", "", "field_56f70dd7b85e4", "", "", "2016-03-26 22:37:07", "2016-03-26 22:37:07", "", "52", "http://rockware:8888/?post_type=acf-field&#038;p=59", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("60", "1", "2016-03-26 22:36:24", "2016-03-26 22:36:24", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:25:\"Enter text for the button\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Button text", "was_wir_leisten_button", "publish", "closed", "closed", "", "field_56f70ec8b1031", "", "", "2016-03-26 22:36:24", "2016-03-26 22:36:24", "", "52", "http://rockware:8888/?post_type=acf-field&p=60", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("61", "1", "2016-03-26 22:37:40", "2016-03-26 22:37:40", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-26 22:37:40", "2016-03-26 22:37:40", "", "5", "http://rockware:8888/2016/03/26/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("62", "1", "2016-03-27 19:33:20", "2016-03-27 19:33:20", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"5\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Mit liebe", "mit-liebe", "publish", "closed", "closed", "", "group_56f835153723c", "", "", "2016-03-29 02:23:20", "2016-03-29 02:23:20", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=62", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("63", "1", "2016-03-27 19:33:20", "2016-03-27 19:33:20", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:56:\"Add an image for the background of the mit liebe section\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "Background image", "mit_liebe_background", "publish", "closed", "closed", "", "field_56f83519c4d4e", "", "", "2016-03-27 19:33:20", "2016-03-27 19:33:20", "", "62", "http://rockware:8888/?post_type=acf-field&p=63", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("64", "1", "2016-03-27 19:35:11", "2016-03-27 19:35:11", "", "city", "", "inherit", "open", "closed", "", "city", "", "", "2016-03-27 19:35:11", "2016-03-27 19:35:11", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/city.jpg", "0", "attachment", "image/jpeg", "0");
INSERT INTO `wp_posts` VALUES("65", "1", "2016-03-27 19:35:19", "2016-03-27 19:35:19", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-27 19:35:19", "2016-03-27 19:35:19", "", "5", "http://rockware:8888/2016/03/27/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("66", "1", "2016-03-27 19:35:25", "2016-03-27 19:35:25", "", "Start", "", "inherit", "closed", "closed", "", "5-autosave-v1", "", "", "2016-03-27 19:35:25", "2016-03-27 19:35:25", "", "5", "http://rockware:8888/2016/03/27/5-autosave-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("67", "1", "2016-03-29 01:26:21", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "open", "open", "", "", "", "", "2016-03-29 01:26:21", "0000-00-00 00:00:00", "", "0", "http://rockware:8888/?p=67", "0", "post", "", "0");
INSERT INTO `wp_posts` VALUES("68", "1", "2016-03-29 01:29:59", "2016-03-29 01:29:59", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:13:\"Enter a title\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:38:\"MIT LIEBE AUS DEM <br>HERZEN MÜNCHENS\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Title", "mit_liebe_title", "publish", "closed", "closed", "", "field_56f9da117ae65", "", "", "2016-03-29 01:31:51", "2016-03-29 01:31:51", "", "62", "http://rockware:8888/?post_type=acf-field&#038;p=68", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("69", "1", "2016-03-29 01:29:59", "2016-03-29 01:29:59", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:20:\"Enter paragraph text\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Paragraph", "mit_liebe_paragraph", "publish", "closed", "closed", "", "field_56f9da3d7ae66", "", "", "2016-03-29 01:29:59", "2016-03-29 01:29:59", "", "62", "http://rockware:8888/?post_type=acf-field&p=69", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("70", "1", "2016-03-29 01:29:59", "2016-03-29 01:29:59", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:25:\"Enter text for the button\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:14:\"MEHR ÜBER UNS\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Button text", "mit_liebe_button_text", "publish", "closed", "closed", "", "field_56f9da587ae67", "", "", "2016-03-29 01:29:59", "2016-03-29 01:29:59", "", "62", "http://rockware:8888/?post_type=acf-field&p=70", "3", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("71", "1", "2016-03-29 01:32:15", "2016-03-29 01:32:15", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-29 01:32:15", "2016-03-29 01:32:15", "", "5", "http://rockware:8888/2016/03/29/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("72", "1", "2016-03-29 02:30:09", "2016-03-29 02:30:09", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"5\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Wir rocken", "wir-rocken", "publish", "closed", "closed", "", "group_56f9e75159f42", "", "", "2016-03-29 02:37:46", "2016-03-29 02:37:46", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=72", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("73", "1", "2016-03-29 02:30:09", "2016-03-29 02:30:09", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:43:\"Enter a title. Put orange text in span tags\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:33:\"WIR <span>ROCKEN</span> IHRE WELT\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Title", "wir_title", "publish", "closed", "closed", "", "field_56f9e75dd54d9", "", "", "2016-03-29 02:35:44", "2016-03-29 02:35:44", "", "72", "http://rockware:8888/?post_type=acf-field&#038;p=73", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("74", "1", "2016-03-29 02:30:09", "2016-03-29 02:30:09", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:20:\"Enter paragraph text\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Paragraph", "wir_paragraph", "publish", "closed", "closed", "", "field_56f9e796d54da", "", "", "2016-03-29 02:30:09", "2016-03-29 02:30:09", "", "72", "http://rockware:8888/?post_type=acf-field&p=74", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("75", "1", "2016-03-29 02:30:09", "2016-03-29 02:30:09", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:12:\"Add an image\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "Image", "wir_image", "publish", "closed", "closed", "", "field_56f9e7b5d54db", "", "", "2016-03-29 02:30:09", "2016-03-29 02:30:09", "", "72", "http://rockware:8888/?post_type=acf-field&p=75", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("76", "1", "2016-03-29 02:34:42", "2016-03-29 02:34:42", "", "people", "", "inherit", "open", "closed", "", "people", "", "", "2016-03-29 02:34:42", "2016-03-29 02:34:42", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/people.jpg", "0", "attachment", "image/jpeg", "0");
INSERT INTO `wp_posts` VALUES("77", "1", "2016-03-29 02:34:46", "2016-03-29 02:34:46", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-29 02:34:46", "2016-03-29 02:34:46", "", "5", "http://rockware:8888/2016/03/29/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("78", "1", "2016-03-29 02:37:09", "2016-03-29 02:37:09", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:46:\"Add a description of the image for the alt tag\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:6:\"People\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Image description", "wir_image_alt", "publish", "closed", "closed", "", "field_56f9ea35ff0ae", "", "", "2016-03-29 02:37:46", "2016-03-29 02:37:46", "", "72", "http://rockware:8888/?post_type=acf-field&#038;p=78", "3", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("79", "1", "2016-03-29 02:38:59", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2016-03-29 02:38:59", "0000-00-00 00:00:00", "", "0", "http://rockware:8888/?post_type=acf-field-group&p=79", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("80", "1", "2016-03-29 02:39:10", "2016-03-29 02:39:10", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"5\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Past clients 2", "past-clients-2", "publish", "closed", "closed", "", "group_56f9eace3bd93", "", "", "2016-03-29 02:40:06", "2016-03-29 02:40:06", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=80", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("81", "1", "2016-03-29 02:39:10", "2016-03-29 02:39:10", "a:9:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:12:\"past clients\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:15:\"Add past client\";}", "Past Clients 2", "past_clients_2", "publish", "closed", "closed", "", "field_56f9eace42cd3", "", "", "2016-03-29 02:39:52", "2016-03-29 02:39:52", "", "80", "http://rockware:8888/?post_type=acf-field&#038;p=81", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("82", "1", "2016-03-29 02:39:10", "2016-03-29 02:39:10", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:37:\"Add a grey logo for every past client\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "past client icons", "past_client_icons_2", "publish", "closed", "closed", "", "field_56f9eace45db0", "", "", "2016-03-29 02:39:52", "2016-03-29 02:39:52", "", "81", "http://rockware:8888/?post_type=acf-field&#038;p=82", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("83", "1", "2016-03-29 02:39:10", "2016-03-29 02:39:10", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:19:\"Add the client name\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "past client icon alt", "past_client_icon_alt_2", "publish", "closed", "closed", "", "field_56f9eace45dda", "", "", "2016-03-29 02:39:52", "2016-03-29 02:39:52", "", "81", "http://rockware:8888/?post_type=acf-field&#038;p=83", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("84", "1", "2016-03-29 02:41:31", "2016-03-29 02:41:31", "", "bmw", "", "inherit", "open", "closed", "", "bmw", "", "", "2016-03-29 02:41:31", "2016-03-29 02:41:31", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/bmw.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("85", "1", "2016-03-29 02:41:32", "2016-03-29 02:41:32", "", "google", "", "inherit", "open", "closed", "", "google", "", "", "2016-03-29 02:41:32", "2016-03-29 02:41:32", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/google.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("86", "1", "2016-03-29 02:41:32", "2016-03-29 02:41:32", "", "ibm", "", "inherit", "open", "closed", "", "ibm", "", "", "2016-03-29 02:41:32", "2016-03-29 02:41:32", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/ibm.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("87", "1", "2016-03-29 02:41:33", "2016-03-29 02:41:33", "", "playboy", "", "inherit", "open", "closed", "", "playboy", "", "", "2016-03-29 02:41:33", "2016-03-29 02:41:33", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/playboy.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("88", "1", "2016-03-29 02:41:34", "2016-03-29 02:41:34", "", "siemens", "", "inherit", "open", "closed", "", "siemens", "", "", "2016-03-29 02:41:34", "2016-03-29 02:41:34", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/siemens.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("89", "1", "2016-03-29 02:41:34", "2016-03-29 02:41:34", "", "zurich", "", "inherit", "open", "closed", "", "zurich", "", "", "2016-03-29 02:41:34", "2016-03-29 02:41:34", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/zurich.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("90", "1", "2016-03-29 02:42:02", "2016-03-29 02:42:02", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-29 02:42:02", "2016-03-29 02:42:02", "", "5", "http://rockware:8888/2016/03/29/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("91", "1", "2016-03-29 02:48:41", "2016-03-29 02:48:41", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"5\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Contact form", "contact-form", "publish", "closed", "closed", "", "group_56f9ec3e713f3", "", "", "2016-03-29 02:58:09", "2016-03-29 02:58:09", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=91", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("92", "1", "2016-03-29 02:48:41", "2016-03-29 02:48:41", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:130:\"Enter the first line of the title. On small screens these will be split onto two lines while on large screens they will be on one.\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:14:\"LUST AUF EINEN\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Contact title first line", "contact_title_first_line", "publish", "closed", "closed", "", "field_56f9ec47e7da9", "", "", "2016-03-29 02:52:30", "2016-03-29 02:52:30", "", "91", "http://rockware:8888/?post_type=acf-field&#038;p=92", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("93", "1", "2016-03-29 02:48:41", "2016-03-29 02:48:41", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:131:\"Enter the second line of the title. On small screens these will be split onto two lines while on large screens they will be on one.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:13:\"PRIVATEN GIG?\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Contact title second line", "contact_title_second_line", "publish", "closed", "closed", "", "field_56f9ec84e7daa", "", "", "2016-03-29 02:52:30", "2016-03-29 02:52:30", "", "91", "http://rockware:8888/?post_type=acf-field&#038;p=93", "2", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("94", "1", "2016-03-29 02:48:41", "2016-03-29 02:48:41", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:20:\"Enter paragraph text\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Contact paragraph", "contact_paragraph", "publish", "closed", "closed", "", "field_56f9ece2e7dab", "", "", "2016-03-29 02:52:30", "2016-03-29 02:52:30", "", "91", "http://rockware:8888/?post_type=acf-field&#038;p=94", "3", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("95", "1", "2016-03-29 02:49:14", "2016-03-29 02:49:14", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-29 02:49:14", "2016-03-29 02:49:14", "", "5", "http://rockware:8888/2016/03/29/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("96", "1", "2016-03-29 02:50:24", "2016-03-29 02:50:24", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:25:\"Enter text for the button\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:7:\"ROCK IT\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Button text", "contact_button_text", "publish", "closed", "closed", "", "field_56f9ed554cee6", "", "", "2016-03-29 02:52:30", "2016-03-29 02:52:30", "", "91", "http://rockware:8888/?post_type=acf-field&#038;p=96", "4", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("97", "1", "2016-03-29 02:52:30", "2016-03-29 02:52:30", "a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}", "Background", "contact_background", "publish", "closed", "closed", "", "field_56f9edd5d016a", "", "", "2016-03-29 02:52:30", "2016-03-29 02:52:30", "", "91", "http://rockware:8888/?post_type=acf-field&p=97", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("98", "1", "2016-03-29 02:53:13", "2016-03-29 02:53:13", "", "hands", "", "inherit", "open", "closed", "", "hands", "", "", "2016-03-29 02:53:13", "2016-03-29 02:53:13", "", "5", "http://rockware:8888/wp-content/uploads/2016/03/hands.png", "0", "attachment", "image/png", "0");
INSERT INTO `wp_posts` VALUES("99", "1", "2016-03-29 02:53:19", "2016-03-29 02:53:19", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-29 02:53:19", "2016-03-29 02:53:19", "", "5", "http://rockware:8888/2016/03/29/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("100", "1", "2016-03-29 03:00:02", "2016-03-29 03:00:02", "a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"5\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Subfooter", "subfooter", "publish", "closed", "closed", "", "group_56f9ef4a29c0e", "", "", "2016-03-29 03:05:49", "2016-03-29 03:05:49", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=100", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("101", "1", "2016-03-29 03:00:02", "2016-03-29 03:00:02", "a:9:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:15:\"Add a paragraph\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;}", "First paragraph", "subf_first_paragraph", "publish", "closed", "closed", "", "field_56f9ef5e8eafd", "", "", "2016-03-29 03:05:49", "2016-03-29 03:05:49", "", "100", "http://rockware:8888/?post_type=acf-field&#038;p=101", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("102", "1", "2016-03-29 03:00:02", "2016-03-29 03:00:02", "a:9:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:15:\"Add a paragraph\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;}", "Second paragraph", "subf_second_paragraph", "publish", "closed", "closed", "", "field_56f9ef8f8eafe", "", "", "2016-03-29 03:05:49", "2016-03-29 03:05:49", "", "100", "http://rockware:8888/?post_type=acf-field&#038;p=102", "1", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("103", "1", "2016-03-29 03:01:29", "2016-03-29 03:01:29", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-29 03:01:29", "2016-03-29 03:01:29", "", "5", "http://rockware:8888/2016/03/29/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("104", "1", "2016-03-29 03:01:55", "2016-03-29 03:01:55", "", "Start", "", "inherit", "closed", "closed", "", "5-revision-v1", "", "", "2016-03-29 03:01:55", "2016-03-29 03:01:55", "", "5", "http://rockware:8888/2016/03/29/5-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("105", "1", "2016-03-29 03:11:01", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2016-03-29 03:11:01", "0000-00-00 00:00:00", "", "0", "http://rockware:8888/?post_type=acf-field-group&p=105", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("106", "1", "2016-03-29 03:13:05", "2016-03-29 03:13:05", "a:7:{s:8:\"location\";a:1:{i:0;a:2:{i:0;a:3:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"top_level\";}i:1;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:1:\"5\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}", "Header title", "header-title", "publish", "closed", "closed", "", "group_56f9f27614f78", "", "", "2016-03-29 03:16:25", "2016-03-29 03:16:25", "", "0", "http://rockware:8888/?post_type=acf-field-group&#038;p=106", "0", "acf-field-group", "", "0");
INSERT INTO `wp_posts` VALUES("108", "1", "2016-03-29 03:13:05", "2016-03-29 03:13:05", "a:12:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:11:\"Add a title\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:8:\"readonly\";i:0;s:8:\"disabled\";i:0;}", "Title", "header_title", "publish", "closed", "closed", "", "field_56f9f294f1ee1", "", "", "2016-03-29 03:16:25", "2016-03-29 03:16:25", "", "106", "http://rockware:8888/?post_type=acf-field&#038;p=108", "0", "acf-field", "", "0");
INSERT INTO `wp_posts` VALUES("109", "1", "2016-03-29 03:15:00", "2016-03-29 03:15:00", "", "Über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-29 03:15:00", "2016-03-29 03:15:00", "", "8", "http://rockware:8888/2016/03/29/8-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("110", "1", "2016-03-29 03:17:47", "2016-03-29 03:17:47", "", "Über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-29 03:17:47", "2016-03-29 03:17:47", "", "8", "http://rockware:8888/2016/03/29/8-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("111", "1", "2016-03-29 03:18:46", "2016-03-29 03:18:46", "", "Über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-29 03:18:46", "2016-03-29 03:18:46", "", "8", "http://rockware:8888/2016/03/29/8-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("112", "1", "2016-03-29 03:19:50", "2016-03-29 03:19:50", "", "Über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-29 03:19:50", "2016-03-29 03:19:50", "", "8", "http://rockware:8888/2016/03/29/8-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("113", "1", "2016-03-29 03:20:13", "2016-03-29 03:20:13", "", "Über uns", "", "inherit", "closed", "closed", "", "8-revision-v1", "", "", "2016-03-29 03:20:13", "2016-03-29 03:20:13", "", "8", "http://rockware:8888/2016/03/29/8-revision-v1/", "0", "revision", "", "0");

/* INSERT TABLE DATA: wp_term_relationships */
INSERT INTO `wp_term_relationships` VALUES("1", "1", "0");
INSERT INTO `wp_term_relationships` VALUES("7", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("9", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("16", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("18", "2", "0");
INSERT INTO `wp_term_relationships` VALUES("21", "2", "0");

/* INSERT TABLE DATA: wp_term_taxonomy */
INSERT INTO `wp_term_taxonomy` VALUES("1", "1", "category", "", "0", "1");
INSERT INTO `wp_term_taxonomy` VALUES("2", "2", "nav_menu", "", "0", "5");

/* INSERT TABLE DATA: wp_terms */
INSERT INTO `wp_terms` VALUES("1", "Uncategorized", "uncategorized", "0");
INSERT INTO `wp_terms` VALUES("2", "main", "main", "0");

/* INSERT TABLE DATA: wp_usermeta */
INSERT INTO `wp_usermeta` VALUES("1", "1", "nickname", "tyler");
INSERT INTO `wp_usermeta` VALUES("2", "1", "first_name", "");
INSERT INTO `wp_usermeta` VALUES("3", "1", "last_name", "");
INSERT INTO `wp_usermeta` VALUES("4", "1", "description", "");
INSERT INTO `wp_usermeta` VALUES("5", "1", "rich_editing", "true");
INSERT INTO `wp_usermeta` VALUES("6", "1", "comment_shortcuts", "false");
INSERT INTO `wp_usermeta` VALUES("7", "1", "admin_color", "fresh");
INSERT INTO `wp_usermeta` VALUES("8", "1", "use_ssl", "0");
INSERT INTO `wp_usermeta` VALUES("9", "1", "show_admin_bar_front", "true");
INSERT INTO `wp_usermeta` VALUES("10", "1", "wp_capabilities", "a:1:{s:13:\"administrator\";b:1;}");
INSERT INTO `wp_usermeta` VALUES("11", "1", "wp_user_level", "10");
INSERT INTO `wp_usermeta` VALUES("12", "1", "dismissed_wp_pointers", "");
INSERT INTO `wp_usermeta` VALUES("13", "1", "show_welcome_panel", "1");
INSERT INTO `wp_usermeta` VALUES("14", "1", "session_tokens", "a:1:{s:64:\"1c8d1db8f824a062c81e1323f6419e2575eba7cac6192e6f417950f20ebacd93\";a:4:{s:10:\"expiration\";i:1459387581;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36\";s:5:\"login\";i:1459214781;}}");
INSERT INTO `wp_usermeta` VALUES("15", "1", "wp_dashboard_quick_press_last_post_id", "67");
INSERT INTO `wp_usermeta` VALUES("16", "1", "wp_user-settings", "libraryContent=browse");
INSERT INTO `wp_usermeta` VALUES("17", "1", "wp_user-settings-time", "1458602549");
INSERT INTO `wp_usermeta` VALUES("18", "1", "managenav-menuscolumnshidden", "a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}");
INSERT INTO `wp_usermeta` VALUES("19", "1", "metaboxhidden_nav-menus", "a:1:{i:0;s:12:\"add-post_tag\";}");
INSERT INTO `wp_usermeta` VALUES("20", "1", "nav_menu_recently_edited", "2");
INSERT INTO `wp_usermeta` VALUES("21", "1", "closedpostboxes_page", "a:9:{i:0;s:23:\"acf-group_56f1c0348747f\";i:1;s:23:\"acf-group_56f9f27614f78\";i:2;s:23:\"acf-group_56f7006885a18\";i:3;s:23:\"acf-group_56f7065a41784\";i:4;s:23:\"acf-group_56f835153723c\";i:5;s:23:\"acf-group_56f9e75159f42\";i:6;s:23:\"acf-group_56f9eace3bd93\";i:7;s:23:\"acf-group_56f9ec3e713f3\";i:8;s:23:\"acf-group_56f9ef4a29c0e\";}");
INSERT INTO `wp_usermeta` VALUES("22", "1", "metaboxhidden_page", "a:12:{i:0;s:23:\"acf-group_56f7065a41784\";i:1;s:23:\"acf-group_56f835153723c\";i:2;s:23:\"acf-group_56f9e75159f42\";i:3;s:12:\"revisionsdiv\";i:4;s:23:\"acf-group_56f9eace3bd93\";i:5;s:23:\"acf-group_56f9ec3e713f3\";i:6;s:23:\"acf-group_56f9ef4a29c0e\";i:7;s:10:\"postcustom\";i:8;s:16:\"commentstatusdiv\";i:9;s:11:\"commentsdiv\";i:10;s:7:\"slugdiv\";i:11;s:9:\"authordiv\";}");
INSERT INTO `wp_usermeta` VALUES("23", "1", "meta-box-order_page", "a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:36:\"submitdiv,pageparentdiv,postimagediv\";s:6:\"normal\";s:286:\"acf-group_56f1c0348747f,acf-group_56f9f27614f78,acf-group_56f7006885a18,acf-group_56f7065a41784,acf-group_56f835153723c,acf-group_56f9e75159f42,revisionsdiv,acf-group_56f9eace3bd93,acf-group_56f9ec3e713f3,acf-group_56f9ef4a29c0e,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}");
INSERT INTO `wp_usermeta` VALUES("24", "1", "screen_layout_page", "2");

/* INSERT TABLE DATA: wp_users */
INSERT INTO `wp_users` VALUES("1", "tyler", "$P$BdSg.Ne1MvzdTekt85Jy4aCzhdc1nR.", "tyler", "tyler@known.design", "", "2016-03-20 18:34:15", "", "0", "tyler");

SET FOREIGN_KEY_CHECKS = 1; 

/* Duplicator WordPress Timestamp: 2016-03-29 22:54:43*/
/* DUPLICATOR_MYSQLDUMP_EOF */
